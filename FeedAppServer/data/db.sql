Drop SCHEMA IF EXISTS `FeedApp`;

CREATE SCHEMA IF NOT EXISTS `FeedApp` DEFAULT CHARACTER SET utf8;

CREATE  TABLE IF NOT EXISTS `FeedApp`.`t_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_name` VARCHAR(100) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  `nick_name` VARCHAR(45) NOT NULL ,
  `avata_url` VARCHAR(100) NULL ,
  `create_time` DATETIME NOT NULL ,
  `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  UNIQUE INDEX `nick_name_UNIQUE` (`nick_name` ASC) 
)
ENGINE = InnoDB 
DEFAULT CHARACTER Set = utf8;

CREATE  TABLE IF NOT EXISTS `FeedApp`.`t_category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `category_name` VARCHAR(100) NOT NULL ,
  `icon_url` VARCHAR(100) NOT NULL ,
  `create_time` DATETIME NOT NULL ,
  `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  UNIQUE INDEX `category_name_UNIQUE` (`category_name` ASC) 
)
ENGINE = InnoDB 
DEFAULT CHARACTER Set = utf8;

CREATE  TABLE IF NOT EXISTS `FeedApp`.`t_feed` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `create_time` DATETIME NOT NULL ,
  `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP ,
  `title` VARCHAR(45),
  `user_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL ,
  `content_image_url` VARCHAR(100) NULL,
  `num_like` INT(11) NOT NULL,
  `num_comment` INT(11) NOT NULL,
  `content` TEXT,
  `reference` VARCHAR(45),
  `reference_url` VARCHAR(100),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `categoty_id` (`category_id` ASC),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id` )
    REFERENCES `FeedApp`.`t_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `category_id`
    FOREIGN KEY (`category_id` )
    REFERENCES `FeedApp`.`t_category` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.google.gson.Gson;


public class TestPinterest2 {

	public TestPinterest2() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Gson gson = new Gson();
		// String json =
		// "[{\"id\":7,\"content\":\"hello\",\"authorNickName\":\"Funny Pictures\",\"authorAvata\":null,\"createTime\":1389020256000,\"categoryId\":2,\"contentImageUrl\":\"http://www.scrapsmp3.com/jokes/english-jokes/jokes.jpg\",\"numLike\":0,\"numComment\":0,\"reference\":\"hello\",\"referenceUrl\":\"asdsd.com\"}]";
		String json = "";
		try {
			
			String pinUrl = "http://www.pinterest.com";
			
			/*
			 * <select name="newFeed.category.id" id="form1_newFeed_category_id">
    <option value="1">General Jokes</option>
    <option value="2">Funny Pics</option>
    <option value="3">Kids</option>
    <option value="4">Rage Comics</option>
    <option value="6">Sports</option>
    <option value="7">Animal &amp; Pets</option>
    <option value="8">Man &amp; Woman</option>
    <option value="11">Stay Positive</option>
    <option value="12">Funny Texts</option>
    <option value="13">Stars</option>
    <option value="14">Funny Quotes</option>


</select>
			 */
			int cateId = 13;
			
			String html = MyFile.readFile("input");
			
	        Document doc = Jsoup.parse(html);
	        Elements eles = doc.select(".pinImageWrapper");
	        
	        int size = eles.size();
	        for(int i=0; i<100000 && i<eles.size(); i++) {

	        	System.out.println(i+":"+size);
	        	String pinIndex = eles.get(i).attr("href");
	        	System.out.println(pinIndex);
	        	

	    		try {
	    			
	    		 	Long createTime = System.currentTimeMillis() + (long)(Math.random()*10*24*3600*1000);
					String admin = "http://env-8029380.jelastic.servint.net/administration/pinNewFeed.action?newFeed.category.id="+cateId+"&newFeed.createTime="+createTime+"&newFeed.user.id=1&newFeed.referenceUrl=";

	    			getStringFromServer(admin+pinUrl+pinIndex);
	    		}
	    		catch(Exception e) {
	    		}
	        }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		System.out.print(json);
	}
	
	private static String getStringFromServer(String link) throws Exception {

		URL url = new URL(link);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(30000);
		con.setReadTimeout(30000);
		InputStream is = url.openStream();
		
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			return jsonText;
		} catch (Exception e) {
			throw e;
		} finally {
			is.close();
		}
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.marcy.feedapp.server.entity.Feed;

public class CCJoke {

	public CCJoke() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		Document doc = Jsoup.connect("http://jokes.cc.com/").get();

		int countTopics;
		int countJokes = 0;

		Elements eles = doc.select(".list_horiz li a");

		System.out.println("Start Loading...");
		for (countTopics = 0; countTopics < eles.size(); countTopics++) {

			System.out.print("Adding Topics: " + eles.get(countTopics).text());
			Document subDoc = Jsoup.connect(eles.get(countTopics).attr("href"))
					.get();

			Elements topicsEles = subDoc.select("#mod_44sc10 div ul li a");

			int countTopicJokes;
			for (countTopicJokes = 0; countTopicJokes < topicsEles.size(); countTopicJokes++) {
				
				Element jokeEle = topicsEles.get(countTopicJokes);
				
				System.out.println("Adding Joke No:" + countJokes + " :" + jokeEle.text());
				
				Document jokeDoc = Jsoup.connect(jokeEle.attr("href")).get();
				
				//reference Url
				String referenceUrl = jokeEle.attr("href");
				
				//title
				String title = jokeDoc.select(".bgb").get(1).text();
				
				//content
				StringBuilder builder = new StringBuilder();;
				Elements paras = jokeDoc.select(".content_wrap p");
				for(Element para:paras) {
					builder.append(para.text()).append("\n").append("\n");
				}
				
				builder.deleteCharAt(builder.length()-2);
				builder.deleteCharAt(builder.length()-1);
				builder.deleteCharAt(builder.length()-3);
				String content = builder.toString();
				
				System.out.println("Title: "+title);
				System.out.println("Content:");
				System.out.println(content);
				
				String domain = "http://env-8029380.jelastic.servint.net/administration/addNewFeed?";
				StringBuilder request = new StringBuilder(domain);
				request.append("newFeed.title=").append(URLEncoder.encode(title,"UTF-8")).append("&");
				request.append("newFeed.referenceUrl=").append(URLEncoder.encode(referenceUrl,"UTF-8")).append("&");
				request.append("newFeed.reference=").append("CCJoke").append("&");
				request.append("newFeed.category.id=1").append("&");
				request.append("newFeed.user.id=1").append("&");
				request.append("newFeed.content=").append(URLEncoder.encode(content,"UTF-8"));
				

				getStringFromServer(request.toString());
				
				countJokes++;
				
				if(countJokes >3)
					return;
			}
		}

	}
	
	
	private static String getStringFromServer(String link) throws Exception {

		URL url = new URL(link);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(30000);
		con.setReadTimeout(30000);
		InputStream is = url.openStream();
		
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			return jsonText;
		} catch (Exception e) {
			throw e;
		} finally {
			is.close();
		}
	}
	
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

}

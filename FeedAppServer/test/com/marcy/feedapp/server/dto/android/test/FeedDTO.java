package com.marcy.feedapp.server.dto.android.test;

import java.io.Serializable;
import java.util.Date;

public class FeedDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String content;
	private String authorNickName;
	private String authorAvata;
	private Date createTime;
	private long categoryId = 0;
	private String contentImageUrl;
	private long numLike = 0;
	private long numComment = 0;
	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAuthorAvata() {
		return authorAvata;
	}

	public void setAuthorAvata(String authorAvata) {
		this.authorAvata = authorAvata;
	}

	public String getAuthorNickName() {
		return authorNickName;
	}

	public void setAuthorNickName(String authorNickName) {
		this.authorNickName = authorNickName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getContentImageUrl() {
		return contentImageUrl;
	}

	public void setContentImageUrl(String contentImageUrl) {
		this.contentImageUrl = contentImageUrl;
	}

	public long getNumComment() {
		return numComment;
	}

	public void setNumComment(long numComment) {
		this.numComment = numComment;
	}

	public long getNumLike() {
		return numLike;
	}

	public void setNumLike(long numLike) {
		this.numLike = numLike;
	}


}

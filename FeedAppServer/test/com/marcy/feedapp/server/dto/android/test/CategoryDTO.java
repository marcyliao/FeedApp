package com.marcy.feedapp.server.dto.android.test;

public class CategoryDTO {
	private int id;
	private boolean selected;
	private String name;
	private String iconLink;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconLink() {
		return iconLink;
	}

	public void setIconLink(String iconLink) {
		this.iconLink = iconLink;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof CategoryDTO))
			return false;
		CategoryDTO c = (CategoryDTO) o;

		return (this.id == c.id);
	}

	@Override
	public int hashCode() {
		return this.id;
	}

}

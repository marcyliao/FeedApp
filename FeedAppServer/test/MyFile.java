import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MyFile {

    public static String readFile(String filePath) {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(filePath);
            BufferedReader reader = new BufferedReader(fileReader);
            return readAll(reader);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                fileReader.close();
            } catch (Exception ex) {
            }
        }
    }

    public String read(String filePath) {
        FileInputStream inputstream = null;

        InputStreamReader reader = null;
        BufferedReader br = null;

        try {
            inputstream = new FileInputStream(filePath);

            reader = new InputStreamReader(inputstream, "gb2312");

            br = new BufferedReader(reader);

            return readAll(br);

        } catch (IOException ex2) {
            ex2.printStackTrace();
            return null;
        } finally {
            if (inputstream != null) {
                try {
                    inputstream.close();
                } catch (IOException ex) {
                }
            }

        }

    }

    private static  String readAll(BufferedReader br) throws IOException {
        StringBuilder sb = new StringBuilder();

        String line = null;
        while ((line = br.readLine()) != null) {

            sb.append(line).append("\r\n");
        }

        return sb.toString();
    }
}
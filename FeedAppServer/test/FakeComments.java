import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.entity.Feed;
import com.marcy.feedapp.server.service.FeedService;

public class FakeComments {

	public FakeComments() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	
	static FeedService service;
	public static void main(String[] args) throws Exception {
		

		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		service = (FeedService) ac.getBean("feedService");

		FeedSearchCriteria criteria = new FeedSearchCriteria();
		criteria.setBeforeTime(System.currentTimeMillis()-60*1000);
		criteria.setStart(0);
		criteria.setNum(20);
		List<Feed> feeds = service.getFeeds(criteria);

		addFakeComments(feeds);
		
	}
	private static void addFakeComments(List<Feed> feeds) throws Exception {
		for (int i=0; i<feeds.size(); i++) {
			Feed f = feeds.get(i);
			System.out.println(i);
			try {
				String url = f.getReferenceUrl();
				if (url.contains("http://www.pinterest.com/pin/")) {
					Document doc = Jsoup.connect(url).get();

					Elements eleLikes = doc.select(".buttonText");
					int sumLike = 0;
					int sumNum = 0;
					for (Element e : eleLikes) {
						try {
							sumLike += Integer.parseInt(e.html());
							sumNum++;
						} catch (Exception ex) {
							continue;
						}
					}

					int actualScore=(int) Math.ceil(Math.pow(sumLike,0.7));
					
					if(f.getNumLike() < 4) {
						f.setNumLike(f.getNumLike()+actualScore);
					}
					
					if(f.getNumUnlike() == 0) {
						if(f.getNumLike() < 3)
							f.setNumUnlike((long)(10+7*Math.random()));
						else if(f.getNumLike() < 10)
							f.setNumUnlike((long)(5+7*Math.random()));
						else
							f.setNumUnlike((long)(7*Math.random()));
					}
					
					System.out.println(actualScore + " "+ sumNum+" "+sumLike);
				

					Elements elesContent = doc.select(".commentDescriptionContent");
					
					if (elesContent.size() > 0) {
						Element ele = elesContent.get(0);
						String content = ele.html();

						if (!content.contains("<a")
								&& !content.contains("</a>")
								&& !content.contains("fun")
								&& !content.contains("Fun")
								&& !content.contains("FUN")
								&& !content.contains("Pic")
								&& !content.contains("pic")
								&& !content.contains("Joke")
								&& !content.contains("joke")
								&& !content.contains("JOKE")
								&& !content.contains("quote")
								&& !content.contains("Quote")
								&& !content.contains("QUOTE")
								&& !content.contains("QUOTE")
								&& !content.contains("humour")
								&& !content.contains("LOL")
								&& !content.contains("image")
								&& !content.contains("Image")
								&& !content.contains("Comic")
								&& !content.contains("comic")) {

							System.out.println(content);
							f.setContent(content.replace("&quot;", ""));
						}
					}
				}

			} catch(Exception e) {
				continue;
			}
			
			service.updateFeed(f);
		}
	}
}

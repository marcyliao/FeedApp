import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class Kickasshumor {
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		String link = "http://kickasshumor.com/ajax/alltime/na/";
		for(int i=4460; i<4500; i++) {
			
			System.out.println(i);
			
			String docStr = getStringFromServer(link+i);

//			System.out.println(docStr);
			docStr = docStr.replace("<br />\n", "9@9");
			
			Document doc = Jsoup.parse(docStr);
			String title = doc.select("body div h2").get(0).text().replace("9@9", "\n");
			String content = doc.select("body div p a").get(0).text().replace("9@9", "\n");
			
			
			String domain = "http://env-8029380.jelastic.servint.net/administration/addNewFeed?";
			StringBuilder request = new StringBuilder(domain);
			request.append("newFeed.title=").append(URLEncoder.encode(title,"UTF-8")).append("&");
			request.append("newFeed.referenceUrl=").append(URLEncoder.encode(link+i,"UTF-8")).append("&");
			request.append("newFeed.reference=").append("Kickasshumor").append("&");
			request.append("newFeed.category.id=1").append("&");
			request.append("newFeed.user.id=1").append("&");
			long time = System.currentTimeMillis()+(long)(45*Math.random()*24*3600*1000);
			System.out.print(new Date(time));
			request.append("newFeed.createTime=").append(String.valueOf(time)).append("&");
			request.append("newFeed.content=").append(URLEncoder.encode(content,"UTF-8"));
			

			getStringFromServer(request.toString());
		}

	}
	
	
	private static String getStringFromServer(String link) throws Exception {

		URL url = new URL(link);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(30000);
		con.setReadTimeout(30000);
		InputStream is = url.openStream();
		
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			return jsonText;
		} catch (Exception e) {
			throw e;
		} finally {
			is.close();
		}
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}
}

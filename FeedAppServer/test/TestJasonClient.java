import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.dto.android.test.FeedDTO;

public class TestJasonClient {

	public TestJasonClient() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Gson gson = new Gson();
		// String json =
		// "[{\"id\":7,\"content\":\"hello\",\"authorNickName\":\"Funny Pictures\",\"authorAvata\":null,\"createTime\":1389020256000,\"categoryId\":2,\"contentImageUrl\":\"http://www.scrapsmp3.com/jokes/english-jokes/jokes.jpg\",\"numLike\":0,\"numComment\":0,\"reference\":\"hello\",\"referenceUrl\":\"asdsd.com\"}]";
		String json = "";
		try {

			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setAfterTime(new Date(0).getTime());
			ArrayList<Long> ids = new ArrayList<Long>();
			ids.add(0L);
			ids.add(1L);
			ids.add(2L);
			criteria.setCategoryIds(ids);
			
			String url = "http://localhost:8080/feed/ajaxGetFeeds?"+criteria.toString();
			System.out.println(url);
			json = getStringFromServer(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<FeedDTO> list = gson.fromJson(json,
				new TypeToken<List<FeedDTO>>() {
		}.getType());

		System.out.print(list.size());
	}

	private static String getStringFromServer(String link) throws Exception {

		URL url = new URL(link);
		URLConnection con = url.openConnection();
		con.setConnectTimeout(30000);
		con.setReadTimeout(30000);
		InputStream is = url.openStream();
		
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			return jsonText;
		} catch (Exception e) {
			throw e;
		} finally {
			is.close();
		}
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

}

package com.marcy.feedapp.server.dao;

import java.util.List;

import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.entity.Feed;

public interface FeedDAO {
	Feed getFeed(long id) throws Exception;
	List<Feed> getFeeds(FeedSearchCriteria criteria) throws Exception;
	List<Feed> getLatestFeeds(int start, int num) throws Exception;
	void updateFeed(Feed feed);
	Feed addLike(long id);
	Feed decreaseLike(long id);
	void insertFeed(Feed feed) throws Exception;
	void deleteFeed(Long feedId);
	Feed addUnlike(long id);
	Feed decreaseUnlike(long id);
	Feed addFavourite(long id);
	Feed decreaseFavourite(long id);
	Feed decreaseState(Long feedId);
	Feed addState(Long feedId);
	Feed addComment(Long feedId);
	Feed getFeedFromReferenceUrl(String refUrl) throws Exception;
	void updateRandoms();
	Feed getFeedByState(Long state) throws Exception;
}

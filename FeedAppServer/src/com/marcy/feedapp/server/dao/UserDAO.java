package com.marcy.feedapp.server.dao;

import java.util.List;

import com.marcy.feedapp.server.entity.User;

public interface UserDAO {

	List<User> getAllAdministrators() throws Exception;
}

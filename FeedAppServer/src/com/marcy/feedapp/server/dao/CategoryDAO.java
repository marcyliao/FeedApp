package com.marcy.feedapp.server.dao;

import java.util.List;

import com.marcy.feedapp.server.entity.Category;

public interface CategoryDAO {
	List<Category> getAllCategory() throws Exception;
}

package com.marcy.feedapp.server.dao.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.marcy.feedapp.server.dao.FeedDAO;
import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.entity.Feed;

public class FeedDAOImpl extends HibernateDaoSupport implements FeedDAO {

	@Override
	public List<Feed> getFeeds(FeedSearchCriteria criteria)  throws Exception{

		StringBuilder hql;
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Collection> paramLists = new HashMap<String, Collection>();

		hql = new StringBuilder("from Feed f where ");
		
		// check before time, which is required
		if (criteria.getBeforeTime() != null) {
			hql.append("f.createTime < :beforeTime ");
			params.put("beforeTime", criteria.getBeforeTime());
		} else {
			hql.append("f.createTime < :beforeTime ");
			params.put("beforeTime", new Date().getTime());	
		}
		
		// check after time
		if (criteria.getAfterTime() != null) {
			hql.append("and f.createTime > :afterTime ");
			params.put("afterTime", criteria.getAfterTime());
		}
		
		// check categories
		if (criteria.getCategoryIds() != null) {
			hql.append("and f.category.id in (:categoryIds) ");
			paramLists.put("categoryIds", criteria.getCategoryIds());
		}

		// check state
		if (criteria.getState() != null) {
			hql.append("and f.state <= :state ");
			params.put("state", criteria.getState());
		}
		
		
		if (criteria.getOrderBy() != null && !criteria.getOrderBy().equals("")) {
			if (criteria.getOrderBy().equals("RAND")) {
				hql.append("order by rand(123)");
			} else
				hql.append("order by f." + criteria.getOrderBy() + " desc");
		}
		else {
			hql.append("order by f.createTime desc");
		}
		
		// test
		System.out.println(hql.toString());
		
		Session session = this.getSessionFactory().getCurrentSession();
		
		
		Query query = session.createQuery(hql.toString())
				.setFirstResult(criteria.getStart())
				.setMaxResults(criteria.getNum());

		for (String key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}
		for (String key : paramLists.keySet()) {
			query.setParameterList(key, paramLists.get(key));
		}

		return query.list();
	}
	
	@Override
	public List<Feed> getLatestFeeds(int start,int num) throws Exception {

		StringBuilder hql;

		hql = new StringBuilder("from Feed f order by f.createTime Desc");
		
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery(hql.toString())
				.setFirstResult(start)
				.setMaxResults(num);

		return query.list();
	}

	@Override
	public void insertFeed(Feed feed) throws Exception{
		feed.setRandom(Math.round((Math.random()*1000000)));
		getHibernateTemplate().save(feed);
	}

	@Override
	public void deleteFeed(Long feedId) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("delete from Feed where id=:id");
		query.setLong("id",feedId);
		query.executeUpdate(); 
	}

	@Override
	public void updateFeed(Feed feed) {
		Session session = this.getSessionFactory().getCurrentSession();
		session.update(feed);
	}

	@Override
	public Feed addLike(long id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numLike = f.numLike+1 where id=:id");
		query.setLong("id",id);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)id);
	}

	@Override
	public Feed decreaseLike(long id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numLike = f.numLike-1 where id=:id");
		query.setLong("id",id);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)id);
	}

	@Override
	public Feed addUnlike(long id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numUnlike = f.numUnlike+1 where id=:id");
		query.setLong("id",id);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)id);
	}

	@Override
	public Feed decreaseUnlike(long id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numUnlike = f.numUnlike-1 where id=:id");
		query.setLong("id",id);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)id);
	}

	@Override
	public Feed addFavourite(long id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numFavourite = f.numFavourite+1 where id=:id");
		query.setLong("id",id);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)id);
	}

	@Override
	public Feed decreaseFavourite(long id) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numFavourite = f.numFavourite-1 where id=:id");
		query.setLong("id",id);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)id);
	}

	@Override
	public Feed decreaseState(Long feedId) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.state = f.state-1 where id=:id");
		query.setLong("id",feedId);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)feedId);
	}

	@Override
	public Feed addState(Long feedId) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.state = f.state+1 where id=:id");
		query.setLong("id",feedId);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)feedId);
	}

	@Override
	public Feed getFeedFromReferenceUrl(String refUrl) throws Exception {
		Session session = this.getSessionFactory().getCurrentSession();
		StringBuilder hql = new StringBuilder("from Feed f where f.referenceUrl = :url");
		Query query = session.createQuery(hql.toString());
		query.setParameter("url", refUrl);
		
		List list = query.list();
		if(list != null && !list.isEmpty())
			return (Feed) query.list().get(0);
		
		return null;
	}

	@Override
	public void updateRandoms() {

		Session session = this.getSessionFactory().getCurrentSession();
		session.createSQLQuery("UPDATE `t_feed` SET `random`= 10000*rand()").executeUpdate();
		
	}
	
	@Override
	public Feed addComment(Long feedId) {
		Session session = this.getSessionFactory().getCurrentSession();
		Query query = session.createQuery("update Feed f set f.numComment = f.numComment+1 where id=:id");
		query.setLong("id",feedId);
		query.executeUpdate(); 
		return (Feed) session.get(Feed.class, (Long)feedId);
		
	}

	@Override
	public Feed getFeed(long id) throws Exception {

		Session session = this.getSessionFactory().getCurrentSession();
		Feed f = (Feed)session.get(Feed.class, id);
		return f;
	}

	@Override
	public Feed getFeedByState(Long state) throws Exception {
		Session session = this.getSessionFactory().getCurrentSession();
		StringBuilder hql = new StringBuilder("from Feed f where f.state = :state");
		Query query = session.createQuery(hql.toString());
		query.setParameter("state", state);
		
		List list = query.list();
		if(list != null && !list.isEmpty())
			return (Feed) query.list().get(0);
		
		return null;
	}

}


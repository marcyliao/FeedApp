package com.marcy.feedapp.server.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.marcy.feedapp.server.dao.UserDAO;
import com.marcy.feedapp.server.entity.User;

public class UserDAOImpl extends HibernateDaoSupport implements UserDAO {
	
	@Override
	public List<User> getAllAdministrators() throws Exception{

		List<User> list = getHibernateTemplate().find("from User where role="+User.ADMINISTRATOR);
		return list;
	}


}

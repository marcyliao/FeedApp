package com.marcy.feedapp.server.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.marcy.feedapp.server.dao.CategoryDAO;
import com.marcy.feedapp.server.entity.Category;

public class CategoryDAOImpl extends HibernateDaoSupport implements CategoryDAO{

	@Override
	public List<Category> getAllCategory() throws Exception{

		List<Category> list = getHibernateTemplate().find("from Category");
		return list;
	}

}

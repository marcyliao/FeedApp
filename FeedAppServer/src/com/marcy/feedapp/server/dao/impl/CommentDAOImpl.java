package com.marcy.feedapp.server.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.marcy.feedapp.server.dao.CommentDAO;
import com.marcy.feedapp.server.entity.Comment;

public class CommentDAOImpl extends HibernateDaoSupport implements CommentDAO {

	@Override
	public List<Comment> getComments(long feedId) throws Exception {
		Session session = this.getSessionFactory().getCurrentSession();
		StringBuilder hql = new StringBuilder("from Comment c where c.feed.id = :feedId");
		Query query = session.createQuery(hql.toString());
		query.setParameter("feedId", feedId);
		
		List list = query.list();
		return list;
	}

	@Override
	public void insertComment(Comment comment) throws Exception {
		getHibernateTemplate().save(comment);
	}

}

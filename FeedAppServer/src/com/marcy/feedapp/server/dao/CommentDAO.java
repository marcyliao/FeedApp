package com.marcy.feedapp.server.dao;

import java.util.List;

import com.marcy.feedapp.server.entity.Comment;

public interface CommentDAO {

	List<Comment> getComments(long feedId) throws Exception;
	void insertComment(Comment comment) throws Exception;

}

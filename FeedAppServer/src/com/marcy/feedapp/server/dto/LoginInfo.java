package com.marcy.feedapp.server.dto;

public class LoginInfo {

	private String userName;
	private String password;
	
	public LoginInfo() {
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

package com.marcy.feedapp.server.dto.android;

import java.util.ArrayList;
import java.util.List;

import com.marcy.feedapp.server.dto.android.CategoryDTO;
import com.marcy.feedapp.server.dto.android.FeedDTO;
import com.marcy.feedapp.server.entity.Category;
import com.marcy.feedapp.server.entity.Comment;
import com.marcy.feedapp.server.entity.Feed;

public class AndroidEntityConvertor {

	public AndroidEntityConvertor() {

	}

	public static CommentDTO fromComment(Comment c) {
		CommentDTO dto = new CommentDTO();
		dto.setContent(c.getContent());
		dto.setCreateTime(c.getCreateTime());
		dto.setUpdateTime(c.getUpdateTime());
		dto.setFeedId(c.getFeed().getId());
		dto.setId(c.getId());

		if (c.getReplyComment() != null) {
			dto.setReplyCommentId(c.getReplyComment().getId());
			dto.setReplyCommentUser(c.getReplyComment().getUserName());
			dto.setReplyUserId(c.getReplyComment().getUser().getId());
		}
		
		dto.setUserId(c.getUser().getId());
		dto.setUserName(c.getUserName());
		
		return dto;
	}
	
	public static FeedDTO fromFeed(Feed f) {
		FeedDTO dto = new FeedDTO();
		dto.setId(f.getId());
		dto.setCategoryId(f.getCategory().getId());
		dto.setContent(f.getContent());
		dto.setAuthorAvata(f.getUser().getAvataUrl());
		dto.setContentImageUrl(f.getContentImageUrl());
		dto.setAuthorNickName(f.getUser().getNickName());
		dto.setCreateTime(f.getCreateTime());
		dto.setReference(f.getReference());
		dto.setReferenceUrl(f.getReferenceUrl());
		dto.setNumLike(f.getNumLike());
		dto.setNumComment(f.getNumComment());
		dto.setAuthorId(f.getUser().getId());
		dto.setCategoryName(f.getCategory().getCategoryName());
		dto.setRandom(f.getRandom());
		dto.setState(f.getState());
		dto.setNumUnlike(f.getNumUnlike());
		dto.setNumFavourite(f.getNumFavourite());
		return dto;
	}
	
	public static CategoryDTO fromCategory(Category c){
		CategoryDTO dto = new CategoryDTO();
		dto.setIconLink(c.getIconUrl());
		dto.setName(c.getCategoryName());
		dto.setSelected(false);
		dto.setId(c.getId());
		
		return dto;
	}
	
	public static List<FeedDTO> fromFeedList(List<Feed> feeds) {
		List<FeedDTO> dtos = new ArrayList<FeedDTO>();
		
		for(Feed f: feeds) {
			dtos.add(fromFeed(f));
		}
		
		return dtos;
	}
	
	public static List<CategoryDTO> fromCategoryList(List<Category> categories) {
		List<CategoryDTO> dtos = new ArrayList<CategoryDTO>();
		
		for(Category c: categories) {
			dtos.add(fromCategory(c));
		}
		
		return dtos;
	}
	
	public static List<CommentDTO> fromCommentList(List<Comment> comments) {
		List<CommentDTO> dtos = new ArrayList<CommentDTO>();
		
		for(Comment c: comments) {
			dtos.add(fromComment(c));
		}
		
		return dtos;
	}


}

package com.marcy.feedapp.server.dto.android;

import java.io.Serializable;

public class FeedDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String content;
	private Long createTime;
	private String contentImageUrl;
	private long numLike = 0;
	private long numUnlike = 0;
	private long numFavourite = 0;
	private long state = 0;
	private long numComment = 0;
	private String reference;
	private String referenceUrl;
	

	private String authorNickName;
	private String authorAvata;
	private long authorId;

	private long categoryId = 0;
	private String categoryName;
	private String categoryIcon;
	
	private long random;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getAuthorAvata() {
		return authorAvata;
	}

	public void setAuthorAvata(String authorAvata) {
		this.authorAvata = authorAvata;
	}

	public String getAuthorNickName() {
		return authorNickName;
	}

	public void setAuthorNickName(String authorNickName) {
		this.authorNickName = authorNickName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getContentImageUrl() {
		return contentImageUrl;
	}

	public void setContentImageUrl(String contentImageUrl) {
		this.contentImageUrl = contentImageUrl;
	}

	public long getNumComment() {
		return numComment;
	}

	public void setNumComment(long numComment) {
		this.numComment = numComment;
	}

	public long getNumLike() {
		return numLike;
	}

	public void setNumLike(long numLike) {
		this.numLike = numLike;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReferenceUrl() {
		return referenceUrl;
	}

	public void setReferenceUrl(String referenceUrl) {
		this.referenceUrl = referenceUrl;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryIcon() {
		return categoryIcon;
	}

	public void setCategoryIcon(String categoryIcon) {
		this.categoryIcon = categoryIcon;
	}

	public long getRandom() {
		return random;
	}

	public void setRandom(long random) {
		this.random = random;
	}

	public long getNumUnlike() {
		return numUnlike;
	}

	public void setNumUnlike(long numUnlike) {
		this.numUnlike = numUnlike;
	}

	public long getNumFavourite() {
		return numFavourite;
	}

	public void setNumFavourite(long numFavourite) {
		this.numFavourite = numFavourite;
	}

	public long getState() {
		return state;
	}

	public void setState(long state) {
		this.state = state;
	}
}

package com.marcy.feedapp.server.utility;

import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class JsonHelper {
	/*
	private static ObjectMapper objectMapper = null;
	
	public static Map<String, Object> readMapFromJson(String jasonString) throws Exception{
		objectMapper = new ObjectMapper();
		Map<String, Object> maps = objectMapper.readValue(jasonString, Map.class);
		return maps;
	}
	
	public static String writeMapToJson(Map<String, Object> map)throws Exception{
		objectMapper = new ObjectMapper();
		StringWriter w=new StringWriter();  
		objectMapper.writeValue(w, map);
		return w.toString();
	}
	
	public static String writeObjectToJson(Object obj) throws Exception{
		objectMapper = new ObjectMapper();
		StringWriter w=new StringWriter();  
		objectMapper.writeValue(w, obj);
		return w.toString();
	}
	
	public static String writeListToJson(List<Object> list)throws Exception{
		objectMapper = new ObjectMapper();
		StringWriter w=new StringWriter();  
		objectMapper.writeValue(w, list);
		return w.toString();
	}
	*/
	
	public static Map<String, Object> readMapFromJson(String jasonString) throws Exception{
		Gson gson = new Gson();
		return gson.fromJson(jasonString, new TypeToken<Map<String, Object>>(){}.getType());
	}
	
	public static String writeMapToJson(Map<String, Object> map)throws Exception{
		Gson gson = new Gson();
		return gson.toJson(map).toString();
	}
	
	public static String writeObjectToJson(Object obj) throws Exception{
		Gson gson = new Gson();
		return gson.toJson(obj).toString();
	}
	
	public static String writeListToJson(List<Object> list)throws Exception{
		Gson gson = new Gson();
		return gson.toJson(list).toString();
	}
	
	
}

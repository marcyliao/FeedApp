package com.marcy.feedapp.server.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class HttpHelper {

	public HttpHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public static String getStringFromServer(String link) throws Exception {
		
		URLConnection con = null;
		URL url = null;
		InputStream is = null;
		
		try {
			url = new URL(link);
			con = url.openConnection();
			con.setConnectTimeout(30000);
			con.setReadTimeout(30000);
			is = url.openStream();

			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			return jsonText;
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				is.close();
			} catch (Exception e) {
				throw e;
			}
		}
		
	}
	
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

}

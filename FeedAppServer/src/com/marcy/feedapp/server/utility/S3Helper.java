package com.marcy.feedapp.server.utility;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Helper {

	private final String BUCKET_NAME = "FeedApp";
	
	public void saveImageToS3(BufferedImage image, String key)
			throws IOException {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", os);
		byte[] buffer = os.toByteArray();
		ByteArrayInputStream is = new ByteArrayInputStream(buffer);

		BasicAWSCredentials awsCredentials = AwsAccessManager.getInstance()
				.getAWSCredentials();

		AmazonS3 s3 = new AmazonS3Client(awsCredentials);

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentType("jpg");
		meta.setContentLength(buffer.length);
		s3.putObject(new PutObjectRequest(BUCKET_NAME, key, is, meta));
		s3.setObjectAcl(BUCKET_NAME, key, CannedAccessControlList.PublicRead);

	}

	public BufferedImage getBufferedImageFromS3(String key) throws IOException {

		BasicAWSCredentials awsCredentials = AwsAccessManager.getInstance()
				.getAWSCredentials();

		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		S3Object o = s3.getObject(BUCKET_NAME, key);

		ImageInputStream iin = ImageIO.createImageInputStream(o
				.getObjectContent());
		BufferedImage img = ImageIO.read(iin);

		return img;

	}

	public void s3SaveFile(File file, String key) {
		BasicAWSCredentials awsCredentials = AwsAccessManager.getInstance()
				.getAWSCredentials();

		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		s3.putObject(new PutObjectRequest(BUCKET_NAME, key, file));
		s3.setObjectAcl(BUCKET_NAME, key, CannedAccessControlList.PublicRead);
	}

	public void s3DeleteFile(String key) {
		BasicAWSCredentials awsCredentials = AwsAccessManager.getInstance()
				.getAWSCredentials();

		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		s3.deleteObject(BUCKET_NAME, key);
	}

	public void MoveFile(String fromKey, String toKey) {
		BasicAWSCredentials awsCredentials = AwsAccessManager.getInstance()
				.getAWSCredentials();

		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		s3.copyObject(BUCKET_NAME, fromKey, BUCKET_NAME, toKey);
		s3.deleteObject(BUCKET_NAME, fromKey);
		s3.setObjectAcl(BUCKET_NAME, toKey, CannedAccessControlList.PublicRead);
	}

	public boolean fileExist(String key) {
		BasicAWSCredentials awsCredentials = AwsAccessManager.getInstance()
				.getAWSCredentials();

		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		ObjectListing objects = s3.listObjects(new ListObjectsRequest()
				.withBucketName(BUCKET_NAME).withPrefix(key));
		for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
			if (objectSummary.getKey().equals(key)) {
				return true;
			}
		}
		return false;
	}

}

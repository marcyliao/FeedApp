package com.marcy.feedapp.server.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.marcy.feedapp.server.utility.JsonHelper;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private InputStream inputStream;
	
	protected ActionContext context;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	
	protected void setListJsonToResponse(List list) throws Exception {
		String json = JsonHelper.writeListToJson(list);
		setInputStream(new ByteArrayInputStream(json.getBytes("UTF-8")));
	}
	
	protected void setMapJsonToResponse(Map map) throws Exception {
		String json = JsonHelper.writeMapToJson(map);
		setInputStream(new ByteArrayInputStream(json.getBytes("UTF-8")));
	}
	
	/**
	 * @param inputStream the inputStream to set
	 */
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * @return the inputStream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}
	
	public InputStream getResult(){
		return getInputStream();
	}
}

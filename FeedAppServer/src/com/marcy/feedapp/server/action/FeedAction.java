package com.marcy.feedapp.server.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.dto.android.AndroidEntityConvertor;
import com.marcy.feedapp.server.entity.Feed;
import com.marcy.feedapp.server.service.FeedService;

public class FeedAction extends BaseAction{

	private static final long serialVersionUID = 1L;
	
	private FeedSearchCriteria criteria = new FeedSearchCriteria();
	
	private FeedService feedService;
	
	private Long state;
	public String ajaxGetFeedByState() throws Exception {
		Feed f = feedService.getFeedByState(getState());
		List<Feed> feeds = new ArrayList<Feed>();
		
		if(f != null)
			feeds.add(f);
		
		this.setListJsonToResponse(AndroidEntityConvertor.fromFeedList(feeds));
		return SUCCESS;
	}
	
	public String ajaxGetFeeds() throws Exception {
		criteria.setState(0L);
		List<Feed> feeds = feedService.getFeeds(criteria);
		
		this.setListJsonToResponse(AndroidEntityConvertor.fromFeedList(feeds));
		return SUCCESS;
	}

	private Long id;
	public String ajaxGetFeedById() throws Exception {
		Feed f = feedService.getFeedById(id);
		List<Feed> feeds = new ArrayList<Feed>();
		
		if(f != null)
			feeds.add(f);
		
		this.setListJsonToResponse(AndroidEntityConvertor.fromFeedList(feeds));
		return SUCCESS;
	}
	
	public String addLike() throws Exception{
		Feed f = feedService.addLike(id);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("feed", AndroidEntityConvertor.fromFeed(f));
		
		this.setMapJsonToResponse(result);
		return SUCCESS;
	}
	
	public String decreaseLike() throws Exception{
		Feed f = feedService.decreaseLike(id);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("feed", AndroidEntityConvertor.fromFeed(f));
		
		this.setMapJsonToResponse(result);
		return SUCCESS;
	}
	
	public String addUnlike() throws Exception{
		
		Feed f = feedService.addUnlike(id);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("feed", AndroidEntityConvertor.fromFeed(f));
		
		this.setMapJsonToResponse(result);
		return SUCCESS;
	}
	
	public String decreaseUnlike() throws Exception{
		Feed f = feedService.decreaseUnlike(id);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("feed", AndroidEntityConvertor.fromFeed(f));
		
		this.setMapJsonToResponse(result);
		return SUCCESS;
	}

	public String addFavourite() throws Exception{
		
		Feed f = feedService.addFavourite(id);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("feed", AndroidEntityConvertor.fromFeed(f));
		
		this.setMapJsonToResponse(result);
		return SUCCESS;
	}
	
	public String decreaseFavourite() throws Exception{
		Feed f = feedService.decreaseFavourite(id);
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("feed", AndroidEntityConvertor.fromFeed(f));
		
		this.setMapJsonToResponse(result);
		return SUCCESS;
	}

	public FeedService getFeedService() {
		return feedService;
	}

	public void setFeedService(FeedService feedService) {
		this.feedService = feedService;
	}

	public FeedSearchCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(FeedSearchCriteria criteria) {
		this.criteria = criteria;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}
}

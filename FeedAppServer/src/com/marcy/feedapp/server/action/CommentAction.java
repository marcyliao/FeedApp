package com.marcy.feedapp.server.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.marcy.feedapp.server.dto.android.AndroidEntityConvertor;
import com.marcy.feedapp.server.entity.Comment;
import com.marcy.feedapp.server.service.CommentService;

public class CommentAction extends BaseAction  {

	private static final long serialVersionUID = 5034518170146028206L;
	
	private CommentService commentService;

	public CommentAction() {
	}
	
	private Comment comment;
	public String ajaxAddComment() throws Exception {
		getComment().setCreateTime(System.currentTimeMillis());
		getCommentService().insertComment(getComment());
		Map<String,String> results = new HashMap<String,String>();
		results.put("result", "sucess");
		this.setMapJsonToResponse(results);
		return SUCCESS;
	}
	
	private Long feedId;
	public String ajaxGetComments() throws Exception {
		List<Comment> comments = getCommentService().getComments(feedId);
		this.setListJsonToResponse(AndroidEntityConvertor.fromCommentList(comments));
		return SUCCESS;
	}
	public Long getFeedId() {
		return feedId;
	}
	public void setFeedId(Long feedId) {
		this.feedId = feedId;
	}
	public CommentService getCommentService() {
		return commentService;
	}
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	public Comment getComment() {
		return comment;
	}
	public void setComment(Comment comment) {
		this.comment = comment;
	}
}

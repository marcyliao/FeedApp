package com.marcy.feedapp.server.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.dto.LoginInfo;
import com.marcy.feedapp.server.entity.Category;
import com.marcy.feedapp.server.entity.Feed;
import com.marcy.feedapp.server.entity.User;
import com.marcy.feedapp.server.service.CategoryService;
import com.marcy.feedapp.server.service.FeedService;
import com.marcy.feedapp.server.service.UserService;

public class AdministrationAction extends BaseAction {

	private static final long serialVersionUID = 1L;
	
	private FeedService feedService;
	private CategoryService categoryService;
	private UserService userService;
	
	private List<User> allManagers;
	private List<Category> allCategories;
	
	public String getLoginPage() {
		return SUCCESS;
	}

	private LoginInfo loginInfo;
	public String login(){
        if(loginInfo.getUserName()!= null && loginInfo.getPassword()!=null && loginInfo.getUserName().equals("Marcy") && loginInfo.getPassword().equals("82098979")) {
            Map<String,Object> session = ServletActionContext.getContext().getSession();
            session.put("user", loginInfo.getUserName());
            return SUCCESS;
        }

        addActionError("username and password not match");
        return ERROR;
	}
	
	public String getHome() throws Exception{
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){

            addActionError("please login first");
            return ERROR;
        }

        // prepare display data 
        allCategories = categoryService.getAllCategory();
        allManagers = userService.getAllAdministrators();
        
        return SUCCESS;
	}
	
	public String getHomePinterest() throws Exception{
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){

            addActionError("please login first");
            return ERROR;
        }

        // prepare display data 
        allCategories = categoryService.getAllCategory();
        allManagers = userService.getAllAdministrators();
        
        return SUCCESS;
	}
	
	private void addFakeLikes(List<Feed> feeds) throws Exception {
		for (int i=0; i<feeds.size(); i++) {
			Feed f = feeds.get(i);
			System.out.println(i);
			try {
				String url = f.getReferenceUrl();
				if (url.contains("http://www.pinterest.com/pin/")) {
					Document doc = Jsoup.connect(url).get();

					Elements eleLikes = doc.select(".buttonText");
					int sumLike = 0;
					int sumNum = 0;
					for (Element e : eleLikes) {
						try {
							sumLike += Integer.parseInt(e.html());
							sumNum++;
						} catch (Exception ex) {
							continue;
						}
					}

					int actualScore=(int) Math.ceil(Math.pow(sumLike,0.7));
					if(actualScore>20) {
						actualScore = (int)(20+10*Math.random());
					}
					
					if(f.getNumLike() < 10) {
						f.setNumLike(f.getNumLike()+actualScore);
					}
					
					if(f.getNumUnlike() < 5) {
						if(f.getNumLike() < 3)
							f.setNumUnlike((long)(10+7*Math.random()));
						else if(f.getNumLike() < 15)
							f.setNumUnlike((long)(5+7*Math.random()));
						else
							f.setNumUnlike((long)(7*Math.random()));
					}
					
					System.out.println(actualScore + " "+ sumNum+" "+sumLike);
				

					Elements elesContent = doc.select(".commentDescriptionContent");
					
					if (elesContent.size() > 0) {
						Element ele = elesContent.get(0);
						String content = ele.html();

						if (!content.contains("<a")
								&& !content.contains("</a>")
								&& !content.contains("fun")
								&& !content.contains("Fun")
								&& !content.contains("FUN")
								&& !content.contains("Pic")
								&& !content.contains("pic")
								&& !content.contains("Joke")
								&& !content.contains("joke")
								&& !content.contains("JOKE")
								&& !content.contains("quote")
								&& !content.contains("Quote")
								&& !content.contains("QUOTE")
								&& !content.contains("QUOTE")
								&& !content.contains("humour")
								&& !content.contains("LOL")
								&& !content.contains("image")
								&& !content.contains("Image")
								&& !content.contains("Comic")
								&& !content.contains("comic")) {

							System.out.println(content);
							f.setContent(content.replace("&quot;", ""));
						}
					}
				}

			} catch(Exception e) {
				continue;
			}
			
			feedService.updateFeed(f);
		}
	}
	
	public String addFakeLikes() throws Exception{

		FeedSearchCriteria criteria = new FeedSearchCriteria();
		criteria.setBeforeTime(System.currentTimeMillis()-20*60*1000);
		criteria.setStart(0);
		criteria.setNum(20);
		List<Feed> feeds = feedService.getFeeds(criteria);
		
		addFakeLikes(feeds);
        return SUCCESS;
	}
	
	public String updateRandoms() throws Exception{
		feedService.updateRandoms();
        return SUCCESS;
	}
		
	public String pinNewFeed() throws Exception{
		
		/*
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        */
        
		if(feedService.getFeedFromReferenceUrl(newFeed.getReferenceUrl()) != null)
			return ERROR;
		
        Document doc = Jsoup.connect(newFeed.getReferenceUrl()).get();
        
        Element elImageUrl = doc.select(".pinImage").first();
        newFeed.setContentImageUrl(elImageUrl.attr("src"));
        
        Element elReference = doc.select(".sourceFlagWrapper a").first();
	    if(elReference != null) {
	        newFeed.setReference(elReference.text());
	        //newFeed.setReferenceUrl(elReference.attr("href"));
        }
	    else {
	        newFeed.setReference("www.pinterest.com");
	        newFeed.setReferenceUrl(newFeed.getContentImageUrl());
	    }
	    	
        if(newFeed.getReferenceUrl().startsWith("/")) {
        	//newFeed.setReferenceUrl("www.pinterest.com"+newFeed.getReferenceUrl());
        	newFeed.setReference(newFeed.getReference()+" from Pinterest");
        }
        
		// Initialize the basic data that the user does not need to enter.
        if(newFeed.getCreateTime() == null || newFeed.getCreateTime() == 0L) {
        	newFeed.setCreateTime(new Date().getTime());
        }
        
        newFeed.setUpdateTime(newFeed.getCreateTime());
        newFeed.setNumComment(0L);
        newFeed.setNumLike(0L);
		
        
		feedService.addNewFeed(newFeed);

        // prepare display data 
        allCategories = categoryService.getAllCategory();
        allManagers = userService.getAllAdministrators();
        
		return SUCCESS;
	}
	
	private Feed newFeed;
	public String addNewFeed() throws Exception{
//		Map<String,Object> session = ServletActionContext.getContext().getSession();
//        String username = (String) session.get("user");
//        
//        if(username == null){
//            addActionError("please login first");
//            return ERROR;
//        }
        
		// Initialize the basic data that the user does not need to enter.
        if(newFeed.getCreateTime() == null || newFeed.getCreateTime() == 0L) {
        	newFeed.setCreateTime(new Date().getTime());
        }
        
        newFeed.setUpdateTime(newFeed.getCreateTime());
        newFeed.setNumComment(0L);
        newFeed.setNumLike(0L);
		
        
		feedService.addNewFeed(newFeed);

        // prepare display data 
        allCategories = categoryService.getAllCategory();
        allManagers = userService.getAllAdministrators();
        
		return SUCCESS;
	}
	
	private int page;
	private List<Feed> newFeeds;
	public String getFeedsView() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        
        getAdministativeFeeds();
		
		return SUCCESS;
	}

	private void getAdministativeFeeds() throws Exception {
		FeedSearchCriteria criteria = new FeedSearchCriteria();
        criteria.setNum(20);
        criteria.setStart(page*20);
        criteria.setState(10L);
        
        newFeeds = feedService.getFeeds(criteria);
	}
	
	private Long feedId;
	public String deleteFeed() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        
        feedService.deleteFeed(feedId);
        
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String addLike() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        
        feedService.addLike(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String decreaseLike() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }

        feedService.decreaseLike(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String addUnlike() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        
        feedService.addUnlike(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String decreaseUnlike() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }

        feedService.decreaseUnlike(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String addFavourite() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        
        feedService.addFavourite(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String decreaseFavourite() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }

        feedService.decreaseFavourite(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String addState() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }
        
        feedService.addState(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public String decreaseState() throws Exception {
		Map<String,Object> session = ServletActionContext.getContext().getSession();
        String username = (String) session.get("user");
        
        if(username == null){
            addActionError("please login first");
            return ERROR;
        }

        feedService.decreaseState(feedId);
        getAdministativeFeeds();
        
		return SUCCESS;
	}
	
	public LoginInfo getLoginInfo() {
		return loginInfo;
	}

	public void setLoginInfo(LoginInfo loginInfo) {
		this.loginInfo = loginInfo;
	}

	public Feed getNewFeed() {
		return newFeed;
	}

	public void setNewFeed(Feed newFeed) {
		this.newFeed = newFeed;
	}

	public FeedService getFeedService() {
		return feedService;
	}

	public void setFeedService(FeedService feedService) {
		this.feedService = feedService;
	}

	public List<Category> getAllCategories() {
		return allCategories;
	}

	public void setAllCategories(List<Category> allCategories) {
		this.allCategories = allCategories;
	}

	public List<User> getAllManagers() {
		return allManagers;
	}

	public void setAllManagers(List<User> allManagers) {
		this.allManagers = allManagers;
	}

	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<Feed> getNewFeeds() {
		return newFeeds;
	}

	public void setNewFeeds(List<Feed> newFeeds) {
		this.newFeeds = newFeeds;
	}

	public Long getFeedId() {
		return feedId;
	}

	public void setFeedId(Long feedId) {
		this.feedId = feedId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}

package com.marcy.feedapp.server.action;

import java.util.List;

import com.marcy.feedapp.server.dto.android.AndroidEntityConvertor;
import com.marcy.feedapp.server.entity.Category;
import com.marcy.feedapp.server.service.CategoryService;

public class CategoryAction extends BaseAction {

	private static final long serialVersionUID = 1L;
	private CategoryService categoryService;
	
	
	public String ajaxGetCategories() throws Exception {		
		List<Category> cates = categoryService.getAllCategory();
		this.setListJsonToResponse(AndroidEntityConvertor.fromCategoryList(cates));
		return SUCCESS;
	}


	public CategoryService getCategoryService() {
		return categoryService;
	}


	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

}

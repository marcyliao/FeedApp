package com.marcy.feedapp.server.service.impl;

import java.util.List;

import com.marcy.feedapp.server.dao.UserDAO;
import com.marcy.feedapp.server.entity.User;
import com.marcy.feedapp.server.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO;
	
	@Override
	public List<User> getAllAdministrators() throws Exception {
		return userDAO.getAllAdministrators();
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
}

package com.marcy.feedapp.server.service.impl;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;

import com.marcy.feedapp.server.dao.FeedDAO;
import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.entity.Feed;
import com.marcy.feedapp.server.service.FeedService;
import com.marcy.feedapp.server.utility.S3Helper;

public class FeedServiceImpl implements FeedService{
	
	private FeedDAO feedDAO;
	
	private static String PREFIX_FEED_IMAGE_ORIGIN = "feed_images/o/";
	private static String PREFIX_FEED_IMAGE_SMALL = "feed_images/s/";
	private static String PREFIX_FEED_IMAGE_MEDIUM = "feed_images/m/";
	private static String PREFIX_FEED_IMAGE_LARGE = "feed_images/l/";
	
	@Override
	public List<Feed> getFeeds(FeedSearchCriteria criteria) throws Exception {
		// TODO Auto-generated method stub
		return feedDAO.getFeeds(criteria);
	}

	public FeedDAO getFeedDAO() {
		return feedDAO;
	}

	public void setFeedDAO(FeedDAO feedDAO) {
		this.feedDAO = feedDAO;
	}

	@Override
	public void addNewFeed(Feed feed) throws Exception {
		
		// Insert the feed to database
		feedDAO.insertFeed(feed);
		
		// Initialize the image url
		if(feed.getContentImageUrl() != null && !feed.getContentImageUrl().equals("")) {

			BufferedImage originalImage = ImageIO.read(new URL(feed.getContentImageUrl()));
			BufferedImage originalImageJPG = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics g = originalImageJPG.getGraphics();
			g.drawImage(originalImage, 0, 0, null);
			
			int width = originalImageJPG.getWidth();
			int height = originalImageJPG.getHeight();
			
			@SuppressWarnings("unused")
			float rate = ((float)width)/height;
			
			
			BufferedImage largeImg = Scalr.resize(originalImageJPG,600);
			BufferedImage mediumImg = Scalr.resize(originalImageJPG,400);
			BufferedImage smallImg = Scalr.resize(originalImageJPG,200);
			
			S3Helper s3Helper = new S3Helper();
			s3Helper.saveImageToS3(originalImageJPG, PREFIX_FEED_IMAGE_ORIGIN+feed.getId());
			s3Helper.saveImageToS3(largeImg, PREFIX_FEED_IMAGE_LARGE+feed.getId());
			s3Helper.saveImageToS3(mediumImg, PREFIX_FEED_IMAGE_MEDIUM+feed.getId());
			s3Helper.saveImageToS3(smallImg, PREFIX_FEED_IMAGE_SMALL+feed.getId());
		}
	}

	@Override
	public List<Feed> getLatestFeeds(int start, int num) throws Exception {
		return feedDAO.getLatestFeeds(start, num);
	}

	@Override
	public void deleteFeed(Long feedId) {
		feedDAO.deleteFeed(feedId);
	}

	@Override
	public Feed addLike(long id) {
		return feedDAO.addLike(id);
	}

	@Override
	public Feed decreaseLike(long id) {
		return feedDAO.decreaseLike(id);
	}

	@Override
	public Feed addUnlike(long id) {
		return feedDAO.addUnlike(id);
	}

	@Override
	public Feed decreaseUnlike(long id) {
		return feedDAO.decreaseUnlike(id);
	}

	@Override
	public Feed addFavourite(long id) {
		return feedDAO.addFavourite(id);
	}

	@Override
	public Feed decreaseFavourite(long id) {
		return feedDAO.decreaseFavourite(id);
	}

	@Override
	public Feed addState(Long feedId) {
		return feedDAO.addState(feedId);
		
	}

	@Override
	public Feed decreaseState(Long feedId) {
		return feedDAO.decreaseState(feedId);
		
	}

	@Override
	public Feed getFeedFromReferenceUrl(String url) throws Exception {
		return feedDAO.getFeedFromReferenceUrl(url);
	}

	@Override
	public void updateFeed(Feed feed) throws Exception {
		feedDAO.updateFeed(feed);
		
	}

	@Override
	public void updateRandoms() throws Exception {
		feedDAO.updateRandoms();
		
	}

	@Override
	public Feed getFeedById(Long id) throws Exception {
		return feedDAO.getFeed(id);
	}

	@Override
	public Feed getFeedByState(Long state) throws Exception {
		return feedDAO.getFeedByState(state);
	}
	

}

package com.marcy.feedapp.server.service.impl;

import java.util.List;

import com.marcy.feedapp.server.dao.CommentDAO;
import com.marcy.feedapp.server.dao.FeedDAO;
import com.marcy.feedapp.server.entity.Comment;
import com.marcy.feedapp.server.service.CommentService;

public class CommentServiceImpl  implements CommentService{
	private CommentDAO commentDAO;
	private FeedDAO feedDAO;

	public CommentServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Comment> getComments(long feedId) throws Exception {
		return commentDAO.getComments(feedId);
	}

	@Override
	public void insertComment(Comment comment) throws Exception {
		commentDAO.insertComment(comment);
		feedDAO.addComment(comment.getFeed().getId());
	}

	public CommentDAO getCommentDAO() {
		return commentDAO;
	}

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	public FeedDAO getFeedDAO() {
		return feedDAO;
	}

	public void setFeedDAO(FeedDAO feedDAO) {
		this.feedDAO = feedDAO;
	}

}

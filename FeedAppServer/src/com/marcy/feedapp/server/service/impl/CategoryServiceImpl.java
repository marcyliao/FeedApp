package com.marcy.feedapp.server.service.impl;

import java.util.List;

import com.marcy.feedapp.server.dao.CategoryDAO;
import com.marcy.feedapp.server.entity.Category;
import com.marcy.feedapp.server.service.CategoryService;

public class CategoryServiceImpl implements CategoryService{

	private CategoryDAO categoryDAO;
	
	public CategoryServiceImpl() {
	}

	public CategoryDAO getCategoryDAO() {
		return categoryDAO;
	}

	public void setCategoryDAO(CategoryDAO categoryDAO) {
		this.categoryDAO = categoryDAO;
	}

	@Override
	public List<Category> getAllCategory() throws Exception {
		// TODO Auto-generated method stub
		return categoryDAO.getAllCategory();
	}

}

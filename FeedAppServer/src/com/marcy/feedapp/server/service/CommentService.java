package com.marcy.feedapp.server.service;

import java.util.List;

import com.marcy.feedapp.server.entity.Comment;

public interface CommentService {

	List<Comment> getComments(long feedId) throws Exception;
	void insertComment(Comment comment) throws Exception;
}

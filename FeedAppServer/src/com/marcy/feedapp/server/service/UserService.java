package com.marcy.feedapp.server.service;

import java.util.List;

import com.marcy.feedapp.server.entity.User;

public interface UserService {
	List<User> getAllAdministrators() throws Exception;
}

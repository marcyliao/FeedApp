package com.marcy.feedapp.server.service;

import java.util.List;

import com.marcy.feedapp.server.entity.Category;

public interface CategoryService {
	List<Category> getAllCategory() throws Exception;
}

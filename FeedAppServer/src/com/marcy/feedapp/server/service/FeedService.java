package com.marcy.feedapp.server.service;

import java.util.List;

import com.marcy.feedapp.server.dto.FeedSearchCriteria;
import com.marcy.feedapp.server.entity.Feed;

public interface FeedService {
	List<Feed> getFeeds(FeedSearchCriteria criteria) throws Exception;
	List<Feed> getLatestFeeds(int start,int num) throws Exception;
	Feed getFeedById(Long id) throws Exception;
	Feed getFeedByState(Long state) throws Exception;
	
	Feed getFeedFromReferenceUrl(String url) throws Exception;
	
	void addNewFeed(Feed feed) throws Exception;
	void deleteFeed(Long feedId);
	Feed addLike(long id);
	Feed decreaseLike(long id);
	Feed addUnlike(long id);
	Feed decreaseUnlike(long id);
	Feed addFavourite(long id);
	Feed decreaseFavourite(long id);
	Feed addState(Long feedId);
	Feed decreaseState(Long feedId);
	
	void updateFeed(Feed feed) throws Exception;
	void updateRandoms() throws Exception;
}

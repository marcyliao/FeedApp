package com.marcy.feedapp.server.entity;

import java.util.Set;

public class User {

	// id
	private Long id;
	
	// accounr information
	private String userName;
	private String password;
	
	// user profile
	private String nickName;
	private String avataUrl;

	// time information
	private Long createTime;
	private Long updateTime;
	
	// feeds
	private Set<Feed> feeds;
	
	// role of the user, 1 is administrator, 0 normal user
	private Long role;
	public static Long ADMINISTRATOR = 1L;
	public static Long NORMAL = 0L;
	
	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAvataUrl() {
		return avataUrl;
	}

	public void setAvataUrl(String avata_url) {
		this.avataUrl = avata_url;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Set<Feed> getFeeds() {
		return feeds;
	}

	public void setFeeds(Set<Feed> feeds) {
		this.feeds = feeds;
	}

	public Long getRole() {
		return role;
	}

	public void setRole(Long role) {
		this.role = role;
	}

}

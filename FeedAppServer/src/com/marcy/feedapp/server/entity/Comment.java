package com.marcy.feedapp.server.entity;

public class Comment {

	private Long id;
	private String content;
	private String userName;
	private Long createTime;
	private Long updateTime;

	private User user;
	private Feed feed;
	private Comment replyComment;

	public Comment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Feed getFeed() {
		return feed;
	}

	public void setFeed(Feed feed) {
		this.feed = feed;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public Comment getReplyComment() {
		return replyComment;
	}

	public void setReplayComment(Comment replyComment) {
		this.setReplyComment(replyComment);
	}

	public void setReplyComment(Comment replyComment) {
		this.replyComment = replyComment;
	}

}

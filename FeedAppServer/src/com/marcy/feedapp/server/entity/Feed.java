package com.marcy.feedapp.server.entity;


public class Feed {

	// id
	private Long id;
	
	// time information
	private Long createTime;
	private Long updateTime;
	
	// feed content
	private String title;
	private String reference;
	private String referenceUrl;
	private String content;
	private String contentImageUrl;
	private Long numLike=0L;
	private Long numUnlike=0L;
	private Long numFavourite=0L;
	private Long state=0L;
	private Long numComment=0L;
	
	// foreign key
	private Long userId;
	private Long categoryId;
	private User user;
	private Category category;
	
	// random num
	private Long random;
	
	public Feed() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getNumLike() {
		return numLike;
	}

	public void setNumLike(Long numLike) {
		this.numLike = numLike;
	}

	public Long getNumComment() {
		return numComment;
	}

	public void setNumComment(Long numComment) {
		this.numComment = numComment;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReferenceUrl() {
		return referenceUrl;
	}

	public void setReferenceUrl(String referenceUrl) {
		this.referenceUrl = referenceUrl;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentImageUrl() {
		return contentImageUrl;
	}

	public void setContentImageUrl(String contentImageUrl) {
		this.contentImageUrl = contentImageUrl;
	}

	public Long getRandom() {
		return random;
	}

	public void setRandom(Long random) {
		this.random = random;
	}

	public Long getNumUnlike() {
		return numUnlike;
	}

	public void setNumUnlike(Long numUnlike) {
		this.numUnlike = numUnlike;
	}

	public Long getNumFavourite() {
		return numFavourite;
	}

	public void setNumFavourite(Long numFavourite) {
		this.numFavourite = numFavourite;
	}

	public Long getState() {
		return state;
	}

	public void setState(Long state) {
		this.state = state;
	}
	

}

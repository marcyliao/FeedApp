package com.marcy.feedapps.server.init;

  
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;  
import javax.servlet.ServletContextListener;  

import org.jsoup.Jsoup;
  
public class InitListener implements ServletContextListener { 
  
    public void contextDestroyed(ServletContextEvent sce) {  
        System.out.println("web exit ... ");  
    }  
  
    public void contextInitialized(ServletContextEvent sce) {  
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				try {
					
					Jsoup.connect("http://env-8029380.jelastic.servint.net/administration/addFakeLikes").timeout(5*60*1000).get();
				} catch (IOException e) {
					e.printStackTrace();
				}
				//System.out.print("add fake likes");
			}
		}, 60*1000,5*60*1000);
		
		Timer timer2 = new Timer();
		timer2.schedule(new TimerTask() {
			public void run() {
				try {
					Jsoup.connect("http://env-8029380.jelastic.servint.net/administration/updateRandoms").get();
				} catch (IOException e) {
					e.printStackTrace();
				}
				//System.out.print("updateRandoms");
			}
		}, 60*1000,5*60*1000);
    }  
  
}  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=ISO-8859-1">
		<title>Manager UI</title>
		<style type="text/css">
		body{
			background-image: url("https://www.coursepeer.com/images/bg.png");
			background-color: #9fd5f7;
            font-family: 'Helvetica Nueve', Helvetica, sans-serif;
		}
				
		.user{
			float:right;
			font-size:20px;
			color:white;
		}
		
		h1 {
			color:#3366FF;
			font-size:48px;
			margin-bottom:5px;
		}
		
		table {
			margin:10px;
			padding:5px;
			border: 1px solid black;
			border-collapse:collapse;
			background-color:white;
		}
		
		table th, td {
			border: 1px solid black;
			padding:5px;
		}
		
		table .short{
			width:100px;
		}
		
		table .middle{
			width:200px;
		}
		
		.icon {
			width:50px;
			height:50px;
		}
		
		.logo {
			width:100px;
		}
		
		.navi {
			background-image: url("/NewCanuck/image/navi.png");
			margin-left:10px;
			margin-right:10px;
			margin-top:10px;
			margin-bottom:0px;
			padding:10px 15px;
			border-radius: 10px 10px 0px 0px;
			color:#999;
		}

		.navi a{
			color: white;
			font-size:18px;
			font-weight:300;
			text-decoration: none;
			margin-left:10px;
			margin-right:10px;
		}
		
		.navi .select{
			color: yellow;
			font-size:18px;
			font-weight:300;
			text-decoration: none;
		}
		
		.list {
			margin-top:0px;
		}
		</style>
	</head>
	<body>
		<h1>Feeds Management View</h1>
		<div>
			<a href="/administration/home"> Add New Feed </a> |
			<a href="/administration/homePinterest"> Add New Feed from Pinterest</a> |
			<a href="/administration/feeds"> Current Feeds </a>
		</div >
		<table class="list">
			<thead>
				<tr>
					<th class="short">
						id
					</th>
					<th class="short">
						State
					</th>
					<th class="short">
						Like
					</th>
					<th class="short">
						Unlike
					</th>
					<th class="short">
						Favorite
					</th>
					<th class="short">
						State
					</th>
					<th>
						Image
					</th>
					<th class="short">
						Title
					</th>
					<th>
						Content
					</th>
					<th class="short">
						Create Date
					</th>
					<th class="short">
						Reference
					</th>
					<th class="short">
						ReferenceUrl
					</th>
					<th class="short">
						Category
					</th>
					<th class="short">
						User
					</th>
					<th class="short">
						Operations
					</th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="newFeeds">
					<tr>
						<td>
							<s:property value="id" />
						</td>
						<td>
							<s:property value="state" /> <br>
							
							<a href="/administration/addState?feedId=<s:property value="id" />&page=<s:property value="page" />"> +state</a>  <br>
							
							<a href="/administration/decreaseState?feedId=<s:property value="id" />&page=<s:property value="page" />"> -state</a>
						</td>
						<td>
							<s:property value="numLike" /> <br>
							
							<a href="/administration/addLike?feedId=<s:property value="id" />&page=<s:property value="page" />"> +like</a>  <br>
							
							<a href="/administration/decreaseLike?feedId=<s:property value="id" />&page=<s:property value="page" />"> -like</a>
						</td>
						<td>
							<s:property value="numUnlike" /> <br>
							
							<a href="/administration/addUnlike?feedId=<s:property value="id" />&page=<s:property value="page" />"> +unlike</a>  <br>
							
							<a href="/administration/decreaseUnlike?feedId=<s:property value="id" />&page=<s:property value="page" />"> -unlike</a>
						</td>
						<td>
							<s:property value="numFavourite" /> <br>
							
							<a href="/administration/addFavourite?feedId=<s:property value="id" />&page=<s:property value="page" />"> +fav</a>  <br>
							
							<a href="/administration/decreaseFavourite?feedId=<s:property value="id" />&page=<s:property value="page" />"> -fav</a>
						</td>
						<td>
							<s:property value="state" /> <br>
						</td>
						<td>
							<img  class="logo" src="https://s3.amazonaws.com/FeedApp/feed_images/m/<s:property value="id" />">
						</td>
						<td>
							<s:property value="title" />
						</td>
						<td>
							<s:property value="content" />
						</td>
						<td>
							<s:property value="createTime" />
						</td>
						<td>
							<s:property value="reference" />
						</td>
						<td>
							<s:property value="referenceUrl" />
						</td>
						<td>
							<s:property value="category.categoryName" />
						</td>
						<td>
							<s:property value="User.userName" />
						</td>
						<td>
							<a href="/administration/deleteFeed?feedId=<s:property value="id" />&page=<s:property value="page" />"> Delete Feed </a>
						</td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
		<div>
			<a href="/administration/feeds?page=<s:property value="page-1" />">previous</a>
			<s:property value="page" />
			<a href="/administration/feeds?page=<s:property value="page+1" />">next</a>
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</body>
</html>
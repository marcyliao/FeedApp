
SELECT recent.dom1, IF(denominator=0,0,(numerator*100.0)/denominator) percentage FROM
(
	SELECT mc1.domain dom1, sum(mc1.count) numerator 
	FROM db_mailing.mailing_count mc1 
	WHERE mc1.date >= "2014-03-16" 
	GROUP BY dom1
) recent
JOIN
(
	SELECT mc2.domain dom2, sum(mc2.count) denominator 
	FROM db_mailing.mailing_count mc2 
	GROUP BY dom2
) total 
WHERE recent.dom1 = total.dom2
ORDER BY percentage DESC


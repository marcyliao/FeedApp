package com.feedFramework.home.view.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.feedFramework.R;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.exception.NoFeedsAddedException;
import com.feedFramework.exception.NotUsingException;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.FeedServiceImpl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;
import com.utility.Utility;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public abstract class TabContentFragment extends ContentFragment {

	// widgets
	protected PullToRefreshListView mPullRefreshListView;

	// list of tabs
	protected List<String> tabs = new ArrayList<String>();
	
	protected List<Button> tabButtons = new ArrayList<Button>();

	// list of displays, the order is corresponding to tabs;
	protected List<FeedListDisplay> displays = new ArrayList<FeedListDisplay>();
	protected int currentDisplayIndex = 0;

	protected abstract String getTitle();
	
	public TabContentFragment addDisplay(String tabName, FeedListDisplay display) {
		tabs.add(tabName);
		displays.add(display);
		return this;
	}

	public FeedListDisplay getCurrentDisplay() {
		return displays.get(currentDisplayIndex);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	    setHasOptionsMenu(true);
		return inflater.inflate(R.layout.feed_list_refreshable, null);
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		configDisplaysSetting();
		initImageLoader();
		prepareWidgets();
		prepareDisplayTabs();

		reloadList();
	}

	@Override
	protected int getThemeColor() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onPause() {
		
		FeedListDisplay currentDisplay = getCurrentDisplay();
		if(currentDisplay.getInitBehaviour() == FeedListDisplay.LOAD_CACHE_INIT) {
			int position = mPullRefreshListView.getRefreshableView().getFirstVisiblePosition();

			Log.d("listLocation", position+" store "+currentDisplay.getId());
			
			currentDisplay.storeLocation(position,getActivity());
		}
		
		super.onPause();
	}

	protected abstract void configDisplaysSetting();

	private void prepareDisplayTabs() {
		LinearLayout tabsLayout = (LinearLayout) getView().findViewById(
				R.id.tabOrderby);

		int size = tabs.size();
		for (int i = 0; i < size; i++) {

			Button btn = new Button(getActivity());
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
					LayoutParams.MATCH_PARENT, 1.0f);
			params.setMargins(1, 0, 0, 1);
			btn.setLayoutParams(params);
			btn.setTextSize(16);
			btn.setBackgroundResource(R.drawable.button_white);
			btn.setText(tabs.get(i));
			btn.setTag(displays.get(i));
			btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					
					//add
					FeedListDisplay currentDisplay = getCurrentDisplay();
					if(currentDisplay.getInitBehaviour() == FeedListDisplay.LOAD_CACHE_INIT) {
						int position = mPullRefreshListView.getRefreshableView().getFirstVisiblePosition();

						Log.d("listLocation", position+" store "+currentDisplay.getId());
						
						currentDisplay.storeLocation(position,getActivity());
					}

					FeedListDisplay nextDisplay = (FeedListDisplay) view.getTag();
					currentDisplayIndex = displays.indexOf(nextDisplay);

					reloadList();
					refreshTabButtonsUI();

				}
			});
			
			tabButtons.add(btn);
			tabsLayout.addView(btn);
		}
		
		refreshTabButtonsUI();
	}
	
	private void refreshTabButtonsUI(){
		for(int i=0; i<tabs.size(); i++) {
			if(i == currentDisplayIndex) {
				tabButtons.get(i).setTextColor(getThemeColor());
			}
			else {
				tabButtons.get(i).setTextColor(Color.BLACK);
			}
		}
	}

	private void reloadList() {
		if (getCurrentDisplay().getInitBehaviour() == FeedListDisplay.LOAD_CACHE_INIT) {
			reLoadFromCache();
		} else if (getCurrentDisplay().getInitBehaviour() == FeedListDisplay.LOAD_NETWORK_INIT) {
			reloadFromNetwork();
		}
	}

	private void reloadFromNetwork() {
		textViewInitLoading.setVisibility(TextView.VISIBLE);
		textViewInitLoading.setText("Loading from network...");
		mPullRefreshListView.getRefreshableView().setSelection(0);

		new GetFeesTask().execute(GetFeesTask.GET_PULL_DOWN_FEEDS);
	}

	private void reLoadFromCache() {
		textViewInitLoading.setVisibility(TextView.VISIBLE);
		textViewInitLoading.setText("Loading from storage...");

		Utility.executeAsyncTask(new AsyncTask<Object, Object, List<Feed>>() {
			@Override
			protected void onPostExecute(List<Feed> result) {

				textViewInitLoading.setVisibility(TextView.GONE);
				if (result != null && result.size() != 0) {
					mFeedList = new LinkedList<Feed>(result);
					mAdapter.notifyDataSetChanged();
				} 
				else if( result != null && result.size() == 0) {
					reloadFromNetwork();
				}
				
				mPullRefreshListView.onRefreshComplete();

				//add
				FeedListDisplay currentDisplay = getCurrentDisplay();
				if(currentDisplay.getInitBehaviour() == FeedListDisplay.LOAD_CACHE_INIT) {
					final int position = currentDisplay.getStoredLocation(getActivity());
					mPullRefreshListView.getRefreshableView().post(new Runnable(){

						@Override
						public void run() {
							mPullRefreshListView.getRefreshableView().setSelection(position);
							
						}
						
					});
					

					Log.d("listLocation", position+" set "+currentDisplay.getId());
				}
				
				super.onPostExecute(result);
			}

			@Override
			protected List<Feed> doInBackground(Object... params) {

				FeedService fs = new FeedServiceImpl();

				if (getActivity() != null) {
					return fs.loadCacheFeeds(getActivity(), getCurrentDisplay()
							.getId());
				}
				return null;
			}
		});

	}

	private void prepareWidgets() {
		// This loading view shows when list view is loading from cache
		textViewInitLoading = (TextView) getView().findViewById(
				R.id.textViewLoadCache);
		textViewInitLoading.setVisibility(TextView.GONE);

		mPullRefreshListView = (PullToRefreshListView) getView().findViewById(
				R.id.pull_refresh_list);

		initPullToRefreshList();
	}

	private void initPullToRefreshList() {
		mPullRefreshListView.setMode(Mode.BOTH);
		mPullRefreshListView
				.setOnRefreshListener(new OnRefreshListener2<ListView>() {

					String label = DateUtils.formatDateTime(getView()
							.getContext(), System.currentTimeMillis(),
							DateUtils.FORMAT_SHOW_TIME
									| DateUtils.FORMAT_SHOW_DATE
									| DateUtils.FORMAT_ABBREV_ALL);

					@Override
					public void onPullDownToRefresh(
							PullToRefreshBase<ListView> refreshView) {

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy()
								.setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						Utility.executeAsyncTask(new GetFeesTask(),GetFeesTask.GET_PULL_DOWN_FEEDS);
					}

					@Override
					public void onPullUpToRefresh(
							PullToRefreshBase<ListView> refreshView) {

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy()
								.setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						Utility.executeAsyncTask(new GetFeesTask(),GetFeesTask.GET_PULL_UP_FEEDS);
					}
				});

		// Need to use the Actual ListView when registering for Context Menu
		ListView actualListView = mPullRefreshListView.getRefreshableView();
		registerForContextMenu(actualListView);

		// Setup Stop loading picture while scrolling
		boolean pauseOnScroll = true;
		boolean pauseOnFling = true;
		mPullRefreshListView.setOnScrollListener(new PauseOnScrollListener(
				imageLoader, pauseOnScroll, pauseOnFling));

		// register adapter
		mAdapter = new FeedListAdapter(getView().getContext());
		actualListView.setAdapter(mAdapter);
	}

	private class GetFeesTask extends
			AsyncTask<Integer, Void, Map<String, Object>> {

		static final int GET_PULL_DOWN_FEEDS = 0;
		static final int GET_PULL_UP_FEEDS = 1;

		@Override
		protected Map<String, Object> doInBackground(Integer... params) {

			Map<String, Object> results = new HashMap<String, Object>();
			LinkedList<Feed> newFeeds = null;
			String msg = null;
			String result = "fail";

			try {
				if (params[0].equals(GET_PULL_DOWN_FEEDS)) {
					newFeeds = getCurrentDisplay().loadFeedsOnPullDownFromTop();
					msg = "Latest Feeds Added";
				} else /* if (params[0].equals(GET_NEXT_OLD_FEEDS)) */{
					newFeeds = getCurrentDisplay().loadFeedsOnPullUpFromBottom();
					msg = "Feeds Added";
				}

				results.put("list", newFeeds);
				result = "success";

			} catch (NetworkException e) {
				msg = "Network error, please check your internet connection";
			} catch (NoFeedsAddedException e) {
				msg = "No more feeds";
			} catch (NotUsingException e) {
				msg = null;
			}

			results.put("msg", msg);
			results.put("result", result);

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(Map<String, Object> results) {
			if (results.get("result").equals("success")) {
				mFeedList = (LinkedList<Feed>) results.get("list");
				mAdapter.notifyDataSetChanged();
			}

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			// TODO: use cool toast :)
			if (results.get("msg") != null && getActivity() != null
					&& results != null && results.get("msg") != null) {
				Toast.makeText(getActivity(),
						(CharSequence) results.get("msg"), Toast.LENGTH_SHORT)
						.show();
			}

			textViewInitLoading.setVisibility(View.GONE);
			super.onPostExecute(results);
		}
	}

	public void refresh() {
		// Manual refresh
		mPullRefreshListView.setRefreshing();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			refresh();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


	
}

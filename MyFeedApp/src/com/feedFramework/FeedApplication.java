package com.feedFramework;

import android.app.Application;

import com.appbrain.AppBrain;

public class FeedApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
        AppBrain.initApp(this);   
	}
}

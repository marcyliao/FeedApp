package com.feedFramework.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.feedFramework.entity.Feed;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class FeedDAO {
	public static final int FAVOURITE = 0;
	public static final int ALL_LATEST = 1;
	public static final int ALL_POPULAR = 2;
	public static final int ALL_RANDOM = 3;
	public static final int MY_SELECTION_LATEST = 4;
	public static final int MY_SELECTION_POPULAR = 5;
	public static final int MY_SELECTION_RANDOM = 6;
	
	public static final int LIKE = 7;
	public static final int UNLIKE = 8;
	
	public static final int ALL_COMMENT=9;
	public static final int MY_SELECTION_COMMENT=10;
	
	private static final String TABLE_NAME = "feed";
	private static final String[] TABLE_INDEX = {"_favourite", "_all_latest", "_all_popular", "_all_random", "_my_selection_latest", "_my_selection_popular", "_my_selection_random", "_like", "_unlike", "_all_comment", "_my_selection_comment"};
	private SQLiteDatabase database = null;
	private SQLiteHelper dbHelper = null;

	final public static String[] ALL_COLUMNS = { "_id", "category_id", "user_icon_url",
			"content", "user_nick_name", "content_image_url",
			"num_comment", "num_like", "create_time","reference","reference_url","user_id","category_name","category_icon_url","cache_time","random","num_unlike"};

	public FeedDAO(Context context) {
		dbHelper = new SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void insertFeed(int sectionId, Feed feed) {
		this.open();
		feed.setCacheTime(new Date().getTime());
		ContentValues values = getContentValuesFromFeed(feed);
		database.insert(TABLE_NAME+TABLE_INDEX[sectionId], null, values);
		this.close();
	}

	public Feed getFeed(Long id, int sectionId) {
		Feed feed = null;
		this.open();
		Cursor cursor = database.query(TABLE_NAME+TABLE_INDEX[sectionId], ALL_COLUMNS, "_id=?",
				new String[] { id.toString() }, null, null, null);

		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			feed = cursorToFeed(cursor,true);
		}

		cursor.close();
		this.close();
		return feed;
	}
	
	public void resetCacheFeeds(int sectionId, List<Feed> feeds) {
		this.open();
		database.delete(TABLE_NAME+TABLE_INDEX[sectionId], null, null);
		
		for(Feed f : feeds) {
			f.setCacheTime(new Date().getTime());
			ContentValues values = getContentValuesFromFeed(f);
			database.insert(TABLE_NAME+TABLE_INDEX[sectionId], null, values);
		}
		this.close();
	}
	
	public void addCacheFeeds(int sectionId, List<Feed> feeds) {
		this.open();
		
		for(Feed f : feeds) {
			f.setCacheTime(new Date().getTime());
			ContentValues values = getContentValuesFromFeed(f);
			database.insert(TABLE_NAME+TABLE_INDEX[sectionId], null, values);
		}
		this.close();
	}

	public void deleteFeed(int sectionId, Long id) {
		this.open();
		database.delete(TABLE_NAME+TABLE_INDEX[sectionId], "_id=?", new String[] { id.toString() });
		this.close();
	}
	
	public List<Feed> getCacheFeeds(int section) {
		this.open();
		List<Feed> feeds = new ArrayList<Feed>();

		Cursor cursor = database.query(TABLE_NAME+TABLE_INDEX[section], ALL_COLUMNS, null, null,
				null, null, "cache_time ASC");

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Feed f = cursorToFeed(cursor,false);
			feeds.add(f);
			cursor.moveToNext();
		}

		cursor.close();
		this.close();
		return feeds;
	}

	private ContentValues getContentValuesFromFeed(Feed feed) {
		ContentValues values = new ContentValues();
		values.put(ALL_COLUMNS[0], feed.getId());
		values.put(ALL_COLUMNS[1], feed.getCategoryId());
		values.put(ALL_COLUMNS[2], feed.getAuthorAvata());
		values.put(ALL_COLUMNS[3], feed.getContent());
		values.put(ALL_COLUMNS[4], feed.getAuthorNickName());
		values.put(ALL_COLUMNS[5], feed.getContentImageUrl());
		values.put(ALL_COLUMNS[6], feed.getNumComment());
		values.put(ALL_COLUMNS[7], feed.getNumLike());
		values.put(ALL_COLUMNS[8], feed.getCreateTime());
		values.put(ALL_COLUMNS[9], feed.getReference());
		values.put(ALL_COLUMNS[10], feed.getReferenceUrl());
		values.put(ALL_COLUMNS[11], feed.getAuthorId());
		values.put(ALL_COLUMNS[12], feed.getCategoryName());
		values.put(ALL_COLUMNS[13], feed.getCategoryIcon());
		values.put(ALL_COLUMNS[14], feed.getCacheTime());
		values.put(ALL_COLUMNS[15], feed.getRandom());
		values.put(ALL_COLUMNS[16], feed.getNumUnlike());
		return values;
	}
	
	// Parameter weak: control loading the big data or not
	private Feed cursorToFeed(Cursor cursor, boolean weak) {
		Feed f = new Feed();
		f.setId(cursor.getLong(0));
		f.setCategoryId(cursor.getLong(1));
		f.setAuthorAvata(cursor.getString(2));
		f.setContent(cursor.getString(3));
		f.setAuthorNickName(cursor.getString(4));
		f.setContentImageUrl(cursor.getString(5));
		f.setNumComment(cursor.getLong(6));
		f.setNumLike(cursor.getLong(7));
		f.setCreateTime(cursor.getLong(8));
		f.setReference(cursor.getString(9));
		f.setReferenceUrl(cursor.getString(10));
		f.setAuthorId(cursor.getLong(11));
		f.setCategoryName(cursor.getString(12));
		f.setCategoryIcon(cursor.getString(13));
		f.setCacheTime(cursor.getLong(14));
		f.setRandom(cursor.getLong(15));
		f.setNumUnlike(cursor.getLong(16));
		return f;
	}


	
}

package com.feedFramework.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "feed.db";

	// Increase version to call onUpdate.
	private static final int DATABASE_VERSION = 10;

	private static final String DATABASE_FEED_PROPERTIES_SQL
			= " create_time integer not null,"
			+ " category_id integer,"
			+ " user_icon_url text,"
			+ " content text,"
			+ " user_nick_name text not null,"
			+ " content_image_url text,"
			+ " num_comment integer not null,"
			+ " num_like integer not null,"
			+ " num_unlike integer not null,"
			+ " reference text,"
			+ " reference_url text,"
			+ " user_id integer not null,"
			+ " category_name text not null,"
			+ " category_icon_url text,"
			+ " cache_time not null,"
			+ " random not null"
			+ ");";

	private static final String DATABASE_CREATE_FEED_FAVIOURITE = "create table feed_favourite (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_MY_SELECTION_LATEST_CACHE = "create table feed_my_selection_latest (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_MY_SELECTION_POPULAR_CACHE = "create table feed_my_selection_popular (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_MY_SELECTION_RANDOM_CACHE = "create table feed_my_selection_random (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_MY_SELECTION_COMMENT_CACHE = "create table feed_my_selection_comment (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_ALL_LATEST_CACHE = "create table feed_all_latest (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_ALL_POPULAR_CACHE = "create table feed_all_popular (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_ALL_RANDOM_CACHE = "create table feed_all_random (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_ALL_COMMENT_CACHE = "create table feed_all_comment (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;
	
	private static final String DATABASE_CREATE_FEED_LIKE_CACHE = "create table feed_like (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;

	private static final String DATABASE_CREATE_FEED_UNLIKE_CACHE = "create table feed_unlike (_id integer primary key not null,"
			+ DATABASE_FEED_PROPERTIES_SQL;

	private static final String DATABASE_DROP_FEED_FAVIOURITE = "DROP TABLE IF EXISTS feed_favourite";
	private static final String DATABASE_DROP_FEED_ALL_LATEST_CACHE = "DROP TABLE IF EXISTS feed_all_latest";
	private static final String DATABASE_DROP_FEED_ALL_POPULAR_CACHE = "DROP TABLE IF EXISTS feed_all_popular";
	private static final String DATABASE_DROP_FEED_ALL_RANDOM_CACHE = "DROP TABLE IF EXISTS feed_all_random";
	private static final String DATABASE_DROP_FEED_MY_SELECTION_LATEST_CACHE = "DROP TABLE IF EXISTS feed_my_selection_latest";
	private static final String DATABASE_DROP_FEED_MY_SELECTION_POPULAR_CACHE = "DROP TABLE IF EXISTS feed_my_selection_popular";
	private static final String DATABASE_DROP_FEED_MY_SELECTION_RANDOM_CACHE = "DROP TABLE IF EXISTS feed_my_selection_random";
	private static final String DATABASE_DROP_FEED_ALL_COMMENT_CACHE = "DROP TABLE IF EXISTS feed_all_comment";
	private static final String DATABASE_DROP_FEED_MY_SELECTION_COMMENT_CACHE = "DROP TABLE IF EXISTS feed_my_selection_comment";
	private static final String DATABASE_DROP_FEED_LIKE_CACHE = "DROP TABLE IF EXISTS feed_like";
	private static final String DATABASE_DROP_FEED_UNLIKE_CACHE = "DROP TABLE IF EXISTS feed_unlike";
	
	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_FEED_FAVIOURITE);
		database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_LATEST_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_POPULAR_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_RANDOM_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_ALL_LATEST_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_ALL_POPULAR_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_ALL_RANDOM_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_LIKE_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_UNLIKE_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_ALL_COMMENT_CACHE);
		database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_COMMENT_CACHE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		try{
			database.execSQL(DATABASE_DROP_FEED_ALL_LATEST_CACHE);
			database.execSQL(DATABASE_DROP_FEED_ALL_POPULAR_CACHE);
			database.execSQL(DATABASE_DROP_FEED_ALL_RANDOM_CACHE);
			database.execSQL(DATABASE_DROP_FEED_MY_SELECTION_LATEST_CACHE);
			database.execSQL(DATABASE_DROP_FEED_MY_SELECTION_POPULAR_CACHE);
			database.execSQL(DATABASE_DROP_FEED_MY_SELECTION_RANDOM_CACHE);
			database.execSQL(DATABASE_DROP_FEED_LIKE_CACHE);
			database.execSQL(DATABASE_DROP_FEED_UNLIKE_CACHE);
			database.execSQL(DATABASE_DROP_FEED_ALL_COMMENT_CACHE);
			database.execSQL(DATABASE_DROP_FEED_MY_SELECTION_COMMENT_CACHE);
		} 
		catch(Exception e) {
			
		}
		

		try{
			database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_LATEST_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_POPULAR_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_RANDOM_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_ALL_LATEST_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_ALL_POPULAR_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_ALL_RANDOM_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_LIKE_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_UNLIKE_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_ALL_COMMENT_CACHE);
			database.execSQL(DATABASE_CREATE_FEED_MY_SELECTION_COMMENT_CACHE);
		} 
		catch(Exception e) {
			
		}
		
	}

}

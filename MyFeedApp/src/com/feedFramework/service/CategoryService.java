package com.feedFramework.service;

import java.util.List;

import android.content.Context;

import com.feedFramework.entity.Category;
import com.feedFramework.exception.NetworkException;

public interface CategoryService {

	public static final String SERVER_URL = "http://env-8029380.jelastic.servint.net/category/ajaxGetCategories";
	public static final String ICON_URL = "https://s3.amazonaws.com/FeedApp/category_icons/";
	
	List<Category> getAllCategories(Context context);
	List<Category> getSelelctedCategories(Context context);
	List<Category> getNotSelectedCategories(Context context);
	List<Category> updateAllCategories(Context context) throws NetworkException;
	void storeCategoriesSetting(Context context, List<Category> cates);
}

package com.feedFramework.service;

import java.util.List;

import com.feedFramework.entity.Comment;
import com.feedFramework.exception.NetworkException;

public interface CommentService {

	public static final String SERVER_URL = "http://env-8029380.jelastic.servint.net/comment/";
	public static final String GET_FEEDS_URL = SERVER_URL+"ajaxGetComments?";
	public static final String Add_FEED_URL = SERVER_URL+"ajaxAddComment?";
	public static final String Add_FEED_URL2 = SERVER_URL+"ajaxAddComment";
	
	void addComment(Comment c) throws NetworkException;
	List<Comment> getComments(long feedId) throws NetworkException;
}

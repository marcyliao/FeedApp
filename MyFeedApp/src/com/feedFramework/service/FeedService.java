package com.feedFramework.service;
import java.util.List;


import android.content.Context;

import com.feedFramework.database.FeedDAO;
import com.feedFramework.entity.Category;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;

public interface FeedService {

	public static final String SEVER_URL = "http://env-8029380.jelastic.servint.net/feed/";
	
	public static final String GET_FEEDS_URL = SEVER_URL+"ajaxGetFeeds?";
	public static final String ADD_LIKE_URL = SEVER_URL+"addLike?";
	public static final String DECREASE_LIKE_URL = SEVER_URL+"decreaseLike?";
	public static final String ADD_UNLIKE_URL = SEVER_URL+"addUnlike?";
	public static final String DECREASE_UNLIKE_URL = SEVER_URL+"decreaseUnlike?";
	public static final String GET_FEED_BY_STATE = SEVER_URL+"ajaxGetFeedByState?state=";
	
	public static final String S3_IMAGE_URL_BASE = "https://s3.amazonaws.com/FeedApp/feed_images/";
	public static final String S3_IMAGE_URL_M = S3_IMAGE_URL_BASE + "m/";
	public static final String S3_IMAGE_URL_L = S3_IMAGE_URL_BASE + "l/";
	public static final String S3_IMAGE_URL_O = S3_IMAGE_URL_BASE + "o/";
	
	public static final int FAVOURITE = FeedDAO.FAVOURITE;
	public static final int ALL_LATEST = FeedDAO.ALL_LATEST;
	public static final int ALL_POPULAR = FeedDAO.ALL_POPULAR;
	public static final int ALL_RANDOM = FeedDAO.ALL_RANDOM;
	public static final int MY_SELECTION_LATEST = FeedDAO.MY_SELECTION_LATEST;
	public static final int MY_SELECTION_POPULAR = FeedDAO.MY_SELECTION_POPULAR;
	public static final int MY_SELECTION_RANDOM = FeedDAO.MY_SELECTION_RANDOM;

	public static final int LIKE = FeedDAO.LIKE;
	public static final int UNLIKE = FeedDAO.UNLIKE;
	
	public static final int ALL_COMMENT = FeedDAO.ALL_COMMENT;
	public static final int MY_SELECTION_COMMENT = FeedDAO.MY_SELECTION_COMMENT;
	
	Feed addLike(long feedId) throws NetworkException ;
	Feed decreaseLike(long feedId) throws NetworkException ;
	
	Feed addUnlike(long feedId) throws NetworkException ;
	Feed decreaseUnlike(long feedId) throws NetworkException ;
	
	// all latest section
	List<Feed> loadLatestFeeds(int num) throws NetworkException;
	List<Feed> loadFeedsBefore(Long lastFeedCreateTime, int num) throws NetworkException;
	
	// all popular section
	List<Feed> loadPopularFeeds(int start, int num) throws NetworkException;
	
	// all random section
	List<Feed> loadRandomFeeds(int start, int num) throws NetworkException;
	
	// all comment section
	List<Feed> loadCommentFeeds(int start, int num) throws NetworkException;
	
	// my selection latest section
	List<Feed> loadLatestFeedsOfCategories(List<Category> cateList, int num) throws NetworkException;
	List<Feed> loadBeforeFeedsOfCategories(List<Category> cateList, Long lastFeedCreateTime, int num) throws NetworkException;
	
	// my selection popular
	List<Feed> loadPopularFeedsOfCategories(List<Category> cateList, int start, int num) throws NetworkException;
	
	// my selection random section
	List<Feed> loadRandomFeedsOfCategories(List<Category> cateList, int start, int num) throws NetworkException;
	
	// my selection comment section
	List<Feed> loadCommentFeedsOfCategories(List<Category> cateList, int start, int num) throws NetworkException;
	
	// load cache feeds
	List<Feed> loadCacheFeeds(Context context, int section);
	
	Feed loadFeedByState(long state) throws NetworkException;
	
	void resetCacheFeeds(Context context, int sectionId, List<Feed> list);
	void addCacheFeeds(Context context, int sectionId, List<Feed> list);
	void addCacheFeed(Context context, int sectionId, Feed f);
	void removeCacheFeed(Context context, int sectionId, long fid);
	boolean hasCacheFeed(Context context, int sectionId, long feedId);
}

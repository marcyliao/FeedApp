package com.feedFramework.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.feedFramework.entity.Category;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.service.CategoryService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utility.Utility;

public class CategoryServiceImpl implements CategoryService{

	private static final String CATEGORIES = "categories";
	
	@Override
	public List<Category> getAllCategories(Context context) {
		Gson gson = new Gson();
		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		String json = sharedPref.getString(CATEGORIES, "[]");
		
		LinkedList<Category> cacheList = gson.fromJson(json, new TypeToken<LinkedList<Category>>(){}.getType());
		return cacheList;
	}

	@Override
	public List<Category> getSelelctedCategories(Context context) {
		List<Category> all = getAllCategories(context);
		
		ArrayList<Category> l = new ArrayList<Category>();
		for (Category c : all) {
			if(c.isSelected())
				l.add(c);
		}
		return l;
	}

	@Override
	public List<Category> getNotSelectedCategories(Context context) {
		List<Category> all = getAllCategories(context);
		
		ArrayList<Category> l = new ArrayList<Category>();
		for (Category c : all) {
			if(!c.isSelected())
				l.add(c);
		}
		return l;
	}

	@Override
	public List<Category> updateAllCategories(Context context) throws NetworkException {
		
		// Obtain the latest category list from network
		String json = Utility.getStringFromServer(SERVER_URL);
		Gson gson = new Gson();
		List<Category> listFromNetwork = gson.fromJson(json, new TypeToken<List<Category>>() {}.getType());
		
		if (listFromNetwork == null)
			return null;
		
		List<Category> selectedList = getSelelctedCategories(context);
		for(Category c: listFromNetwork) {
			
			// Set the categories in list from network to selected true if user
			// have already set them true.
			if(selectedList.contains(c)) {
				c.setSelected(true);
			}
			
			// Set the icon image url
			c.setIconLink(ICON_URL+c.getId());
		}
		
		storeCategoriesSetting(context,listFromNetwork);
		return listFromNetwork;
	}

	@Override
	public void storeCategoriesSetting(Context context, List<Category> cates) {
		
		Gson gson = new Gson();
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		
		// store the new list in cache
		String json2 = gson.toJson(cates);
		sharedPref.edit().putString(CATEGORIES, json2).commit();
		
	}

}

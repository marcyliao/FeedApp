package com.feedFramework.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.feedFramework.database.FeedDAO;
import com.feedFramework.entity.Category;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.CategoryService;
import com.feedFramework.service.FeedService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utility.Utility;

public class FeedServiceImpl implements FeedService {

	@Override
	public Feed addLike(long feedId) throws NetworkException {
		Gson gson = new Gson();

		String json = Utility.getStringFromServer(ADD_LIKE_URL+"id="+feedId);
		Map<String,Feed> result = gson.fromJson(json, new TypeToken<Map<String,Feed>>() {}.getType());
		Feed f = result.get("feed");
		return f;
	}

	@Override
	public Feed decreaseLike(long feedId)  throws NetworkException {
		Gson gson = new Gson();

		String json = Utility.getStringFromServer(DECREASE_LIKE_URL+"id="+feedId);
		Map<String,Feed> result = gson.fromJson(json, new TypeToken<Map<String,Feed>>() {}.getType());
		Feed f = result.get("feed");
		return f;
	}

	@Override
	public Feed addUnlike(long feedId)  throws NetworkException {
		Gson gson = new Gson();

		String json = Utility.getStringFromServer(ADD_UNLIKE_URL+"id="+feedId);
		Map<String,Feed> result = gson.fromJson(json, new TypeToken<Map<String,Feed>>() {}.getType());
		Feed f = result.get("feed");
		return f;
	}

	@Override
	public Feed decreaseUnlike(long feedId)  throws NetworkException {
		Gson gson = new Gson();

		String json = Utility.getStringFromServer(DECREASE_UNLIKE_URL+"id="+feedId);
		Map<String,Feed> result = gson.fromJson(json, new TypeToken<Map<String,Feed>>() {}.getType());
		Feed f = result.get("feed");
		return f;
	}
	
	private List<Feed> loadFeedsFromServerByCriteria(FeedSearchCriteria criteria) throws NetworkException {
		Gson gson = new Gson();

		String json = Utility.getStringFromServer(GET_FEEDS_URL + criteria.toString());
		
		List<Feed> feeds  = gson.fromJson(json, new TypeToken<List<Feed>>() {
		}.getType());

		// load image links
		for (Feed f : feeds) {
			if (f.getContentImageUrl() != null
					&& !f.getContentImageUrl().equals(""))
				f.setContentImageUrl(S3_IMAGE_URL_M + f.getId());
		}

		// load image links
		for (Feed f : feeds) {
			f.setCategoryIcon(CategoryService.ICON_URL + f.getCategoryId());
		}
		
		for (Feed f:feeds) {
			if(f.getState() == -2 && Setting.isFree()) {
				f.setContent("Sorry, this feed is only available on Pro version...");
				f.setContentImageUrl(null);
				continue;
			}
		}
		
		return feeds;
	}
	
	@Override
	public List<Feed> loadLatestFeeds(int num) throws NetworkException {
		try {
			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setBeforeTime(new Date().getTime());
			criteria.setNum(num);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}

	}

	@Override
	public List<Feed> loadPopularFeeds(int start, int num)
			throws NetworkException {
		try {
			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setStart(start);
			criteria.setOrderBy("numLike");
			criteria.setAfterTime(System.currentTimeMillis() - Utility.DAY);
			criteria.setNum(num);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public List<Feed> loadFeedsBefore(Long lastFeedCreateTime, int num)
			throws NetworkException {

		try {
			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setBeforeTime(lastFeedCreateTime);
			criteria.setNum(num);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public List<Feed> loadLatestFeedsOfCategories(List<Category> cateList,
			int num) throws NetworkException {

		try {
			if (cateList == null || cateList.isEmpty())
				return null;

			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setBeforeTime(new Date().getTime());
			criteria.setNum(num);

			List<Long> cateIds = new ArrayList<Long>();
			for (Category c : cateList) {
				cateIds.add(c.getId());
			}
			
			criteria.setCategoryIds(cateIds);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public List<Feed> loadBeforeFeedsOfCategories(List<Category> cateList,
			Long lastFeedCreateTime, int num) throws NetworkException {

		try {
			if (cateList == null || cateList.isEmpty())
				return null;
			
			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setBeforeTime(lastFeedCreateTime);
			criteria.setNum(num);

			List<Long> cateIds = new ArrayList<Long>();
			for (Category c : cateList) {
				cateIds.add(c.getId());
			}

			criteria.setCategoryIds(cateIds);


			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public LinkedList<Feed> loadCacheFeeds(Context context, int section) {
		FeedDAO feedDAO = new FeedDAO(context);
		List<Feed> feeds = feedDAO.getCacheFeeds(section);

		return new LinkedList<Feed>(feeds);
	}

	@Override
	public void resetCacheFeeds(Context context, int section, List<Feed> list) {
		FeedDAO feedDAO = new FeedDAO(context);
		feedDAO.resetCacheFeeds(section, list);
	}

	@Override
	public void addCacheFeeds(Context context, int sectionId, List<Feed> list) {
		FeedDAO feedDAO = new FeedDAO(context);
		feedDAO.addCacheFeeds(sectionId, list);

	}

	@Override
	public void addCacheFeed(Context context, int sectionId, Feed f) {
		FeedDAO feedDAO = new FeedDAO(context);
		feedDAO.insertFeed(sectionId, f);

	}

	@Override
	public boolean hasCacheFeed(Context context, int sectionId, long feedId) {

		FeedDAO feedDAO = new FeedDAO(context);
		return feedDAO.getFeed(feedId, sectionId) != null;
	}

	@Override
	public void removeCacheFeed(Context context, int sectionId, long fid) {
		FeedDAO feedDAO = new FeedDAO(context);
		feedDAO.deleteFeed(sectionId, fid);
	}

	@Override
	public List<Feed> loadPopularFeedsOfCategories(List<Category> cateList,
			int start, int num) throws NetworkException {
		try {
			if (cateList == null || cateList.isEmpty())
				return null;

			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setOrderBy("numLike");
			criteria.setStart(start);
			criteria.setAfterTime(System.currentTimeMillis() - Utility.DAY);
			criteria.setNum(num);

			List<Long> cateIds = new ArrayList<Long>();
			for (Category c : cateList) {
				cateIds.add(c.getId());
			}
			
			criteria.setCategoryIds(cateIds);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public List<Feed> loadRandomFeeds(int start, int num)
			throws NetworkException {
		try {
			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setStart(start);
			criteria.setOrderBy("random");
			criteria.setNum(num);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public List<Feed> loadRandomFeedsOfCategories(List<Category> cateList,
			int start, int num) throws NetworkException {
		try {
			if (cateList == null || cateList.isEmpty())
				return null;

			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setOrderBy("random");
			criteria.setStart(start);
			criteria.setNum(num);

			List<Long> cateIds = new ArrayList<Long>();
			for (Category c : cateList) {
				cateIds.add(c.getId());
			}
			
			criteria.setCategoryIds(cateIds);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public Feed loadFeedByState(long state) throws NetworkException {
		
		Gson gson = new Gson();

		String json = Utility.getStringFromServer(GET_FEED_BY_STATE + state);
		
		List<Feed> feeds  = gson.fromJson(json, new TypeToken<List<Feed>>() {
		}.getType());

		if(feeds != null && feeds.size() != 0) {
			Feed f = feeds.get(0);

			if (f.getContentImageUrl() != null && !f.getContentImageUrl().equals(""))
				f.setContentImageUrl(S3_IMAGE_URL_M + f.getId());

			f.setCategoryIcon(CategoryService.ICON_URL + f.getCategoryId());
			
			return f;
		}
		
		return null;
	}

	@Override
	public List<Feed> loadCommentFeeds(int start, int num)
			throws NetworkException {
		try {
			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setStart(start);
			criteria.setOrderBy("numComment");
			criteria.setNum(num);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}

	@Override
	public List<Feed> loadCommentFeedsOfCategories(List<Category> cateList,
			int start, int num) throws NetworkException {
		try {
			if (cateList == null || cateList.isEmpty())
				return null;

			FeedSearchCriteria criteria = new FeedSearchCriteria();
			criteria.setOrderBy("numComment");
			criteria.setStart(start);
			criteria.setNum(num);

			List<Long> cateIds = new ArrayList<Long>();
			for (Category c : cateList) {
				cateIds.add(c.getId());
			}
			
			criteria.setCategoryIds(cateIds);

			return loadFeedsFromServerByCriteria(criteria);

		} catch (Exception e) {
			throw new NetworkException();
		}
	}
	



}

package com.feedFramework.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.feedFramework.entity.Comment;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.service.CommentService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.utility.Utility;

public class CommentServiceImpl implements CommentService {

	@Override
	public void addComment(Comment c) throws NetworkException {
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("comment.feed.id", String.valueOf(c.getFeedId()));
		data.put("comment.user.id", String.valueOf(c.getUserId()));
		data.put("comment.content", String.valueOf(c.getContent()));
		data.put("comment.userName", String.valueOf(c.getUserName()));
		String json = Utility.postJsonDataToServer(Add_FEED_URL2, data);
		
		if(!json.equals("{\"result\":\"sucess\"}")){
			throw new NetworkException();
		}
	}

	@Override
	public List<Comment> getComments(long feedId) throws NetworkException {
		Gson gson = new Gson();
		String json = Utility.getStringFromServer(GET_FEEDS_URL + "feedId=" + feedId);
		
		List<Comment> comments  = gson.fromJson(json, new TypeToken<List<Comment>>() {
		}.getType());
		
		return comments;
	}

}

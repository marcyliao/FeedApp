package com.feedFramework.service.impl;

import java.util.Date;
import java.util.List;

public class FeedSearchCriteria {

	
	private static String REQUEST_OBJECT_NAME = "criteria";
	private static final Integer DEFAULT_NUM = 10;
	
	private Date beforeTime;
	private Date afterTime;
	
	private String orderBy;
	private Integer num;
	private List<Long> categoryIds;
	
	private Integer start;
	
	public FeedSearchCriteria() {
		setStart(0);
		setNum(DEFAULT_NUM);
		beforeTime = null;
		afterTime = null;
		setOrderBy(null);
		setCategoryIds(null);
	}

	public Date getBeforeTime() {
		return beforeTime;
	}

	public void setBeforeTime(long beforeTime) {
		this.beforeTime = new Date(beforeTime);
	}

	public Date getAfterTime() {
		return afterTime;
	}
	
	public void setAfterTime(long afterTime) {
		this.afterTime = new Date(afterTime);
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<Long> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}
	
	@Override
	public String toString(){
		
		StringBuffer criteria = new StringBuffer();
		criteria.append(REQUEST_OBJECT_NAME).append(".start=").append(start);
		criteria.append("&&");
		criteria.append(REQUEST_OBJECT_NAME).append(".num=").append(num);
		
		if(this.getAfterTime()!=null) {
			criteria.append("&&");
			criteria.append(REQUEST_OBJECT_NAME).append(".afterTime=").append(afterTime.getTime());
		}
		
		if(this.getBeforeTime()!=null) {
			criteria.append("&&");
			criteria.append(REQUEST_OBJECT_NAME).append(".beforeTime=").append(beforeTime.getTime());
		}
		
		if(this.getOrderBy()!=null) {
			criteria.append("&&");
			criteria.append(REQUEST_OBJECT_NAME).append(".orderBy=").append(orderBy);
		}
		
		if (this.getCategoryIds() != null) {
			for (int i = 0; i < categoryIds.size(); i++) {
				criteria.append("&&");
				criteria.append(REQUEST_OBJECT_NAME).append(".categoryIds["+i+"]=").append(this.getCategoryIds().get(i));
			}
		}
			
		return criteria.toString();
	}

}

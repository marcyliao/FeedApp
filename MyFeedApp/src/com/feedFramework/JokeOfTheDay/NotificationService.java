package com.feedFramework.JokeOfTheDay;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import com.feedFramework.R;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.home.view.FeedActivity;
import com.feedFramework.home.view.MainActivity;
import com.feedFramework.service.impl.FeedServiceImpl;

public class NotificationService extends Service {

	private NotificationManager mManager;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		new AsyncTask<Object, Object, Feed>(){

			@Override
			protected void onPostExecute(Feed feeOfTheDay) {
				
				// Getting Notification Service
				Context context = NotificationService.this.getApplicationContext();
				mManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			
				Intent newIntent = null;
				String title = null;
				String content = null;
				if(feeOfTheDay == null) {
					newIntent = new Intent(context, MainActivity.class);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
							| Intent.FLAG_ACTIVITY_CLEAR_TOP);
					title = "FunnyBook";
					content = "More Jokes and funny pics were updated!";	
				}
				else {
					newIntent = new Intent(context, FeedActivity.class);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
							| Intent.FLAG_ACTIVITY_CLEAR_TOP);
					newIntent.putExtra("feed", feeOfTheDay);
					
					title= "Short Joke Today";
					content = feeOfTheDay.getContent();
				}
				
				Notification notification = new Notification(R.drawable.logo_small,content, System.currentTimeMillis());
				
				PendingIntent pendingNotificationIntent = PendingIntent.getActivity(
						context, 0, newIntent,
						PendingIntent.FLAG_UPDATE_CURRENT);

				notification.flags |= Notification.FLAG_AUTO_CANCEL;

				notification.setLatestEventInfo(context,title, content,pendingNotificationIntent);

				mManager.notify(0, notification);
				
				super.onPostExecute(feeOfTheDay);
				
				stopSelf();
			}

			@Override
			protected Feed doInBackground(Object... arg0) {
				Feed feeOfTheDay = null;
				try {
					feeOfTheDay = new FeedServiceImpl().loadFeedByState(-1l);
					return feeOfTheDay;
				} catch (NetworkException e) {
					return null;
				}
			}
			
		}.execute();
		

		return super.onStartCommand(intent,flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
package com.feedFramework.JokeOfTheDay;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {

	public static void startAlarm(Context context) {
		Intent myIntent = new Intent(context, AlarmReceiver.class);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis(), AlarmManager.INTERVAL_DAY,
				pendingIntent);
	}

	public static void cancelAlarm(Context context) {
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		Intent myIntent = new Intent(context, AlarmReceiver.class);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		if (alarmManager != null) {
			alarmManager.cancel(pendingIntent);
		}
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent myIntent = new Intent(context, NotificationService.class);
		context.startService(myIntent);
	}

}
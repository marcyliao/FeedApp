package com.feedFramework.JokeOfTheDay;

import com.feedFramework.preference.Setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			if(Setting.isFeedOfTheDayEnabled(context))
				AlarmReceiver.startAlarm(context);
		}
	}
}
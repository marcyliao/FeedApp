package com.feedFramework.home.view;

import java.util.List;

import com.feedFramework.R;
import com.feedFramework.entity.Comment;
import com.utility.Utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CommetListAdapter extends BaseAdapter{
	
	private Context context;
	private List<Comment> comments;
	private TextView textViewCommentsNum;
	
	public CommetListAdapter(Context context, TextView textViewCommentsNum) {
		this.context = context;
		this.textViewCommentsNum = textViewCommentsNum;
	}
	
	@Override
	public int getCount() {
		return getComments() == null? 0:getComments().size();
	}

	@Override
	public Comment getItem(int position) {
		return getComments() == null? null:getComments().get(position);
	}

	@Override
	public long getItemId(int position) {
		return getComments() == null? 0L:getComments().get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Comment comment = getComments().get(position);
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.comment_list_item, parent,false);
		
		TextView textViewComment = (TextView) v.findViewById(R.id.textViewComment);
		TextView textViewCommentUserName = (TextView) v.findViewById(R.id.textViewCommentUserName);
		TextView textViewCommentTime = (TextView) v.findViewById(R.id.textViewCommentTime);
		
		textViewComment.setText(comment.getContent());
		textViewCommentUserName.setText(comment.getUserName());
		textViewCommentTime.setText(Utility.getTimeDescription(comment.getCreateTime()));
		
		return v;
	}
	
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
		textViewCommentsNum.setText(String.valueOf(getCount()));
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}

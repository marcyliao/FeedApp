package com.feedFramework.home.view;

public interface OnMenuMessageListener {
	void OnLoadAllLatest();
	void OnLoadMySelected();
	void OnLoadFavourite();
}

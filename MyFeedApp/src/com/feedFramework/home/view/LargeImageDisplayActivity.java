package com.feedFramework.home.view;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import uk.co.senab.photoview.PhotoViewAttacher;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.loader.image.ImageLoader;
import com.bumptech.glide.presenter.target.SimpleTarget;
import com.feedFramework.R;
import com.feedFramework.service.FeedService;

public class LargeImageDisplayActivity extends SherlockActivity {

	ImageView imageView;

	PhotoViewAttacher mAttacher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
		setContentView(R.layout.activity_large_image_display);
		
		imageView = (ImageView) findViewById(R.id.imageViewLargePic);
		final ProgressBar spinner = (ProgressBar)findViewById(R.id.progressBar);
		
		
		Long id = getIntent().getLongExtra("id", -1L);

		spinner.setVisibility(View.VISIBLE);
		Glide.load(FeedService.S3_IMAGE_URL_O+id).into(new SimpleTarget() {

			@Override
			public void onImageReady(Bitmap bm) {
				imageView.setImageBitmap(bm);
				if(mAttacher != null)
					mAttacher.update();
				spinner.setVisibility(View.GONE);
			}

			@Override
			protected int[] getSize() {
				// TODO Auto-generated method stub
				return new int[]{800,800};
			}
			
		}).with(this);
		
	    mAttacher = new PhotoViewAttacher(imageView);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.large, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		final long id = getIntent().getLongExtra("id", -1L);
		switch (item.getItemId()) {

		case R.id.action_save:
			Glide.load(FeedService.S3_IMAGE_URL_O + id).into(new SimpleTarget(){

				@Override
				public void onImageReady(Bitmap loadedImage) {
					if(!isExternalStorageWritable()) {
						Toast.makeText(LargeImageDisplayActivity.this, "Error happens.. External storage is not available in this phone.", Toast.LENGTH_SHORT).show();
						return;
					}
					saveBitmap(loadedImage, id);
					
				}

				@Override
				protected int[] getSize() {
					// TODO Auto-generated method stub
					return new int[] {800,800};
				}
				
			}).with(this);
			
			break;
			
		case R.id.action_share:
			Glide.load(FeedService.S3_IMAGE_URL_O + id).into(new SimpleTarget(){

				@Override
				public void onImageReady(Bitmap loadedImage) {
					if(!isExternalStorageWritable()) {
						Toast.makeText(LargeImageDisplayActivity.this, "Error happens.. External storage is not available in this phone.", Toast.LENGTH_SHORT).show();
						return;
					}
					shareBitmap(loadedImage);
					
				}

				@Override
				protected int[] getSize() {
					// TODO Auto-generated method stub
					return new int[] {800,800};
				}
				
			}).with(this);

		}
		return super.onOptionsItemSelected(item);
	}
	
	private void saveBitmap(Bitmap loadedImage, long id) {
		String file_path = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/" + getString(R.string.app_name);
		File dir = new File(file_path);
		if (!dir.exists())
			dir.mkdirs();
		
		String fileName = getString(R.string.app_name) + "-" + id + ".jpeg";
		File file = new File(dir, fileName);
		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(file);
			loadedImage.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
			fOut.flush();
			fOut.close();
			
			Toast.makeText(this, "The image is saved as "+file_path+"/"+fileName, Toast.LENGTH_SHORT).show();
			
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "Error happens. Image could not be saved", Toast.LENGTH_SHORT).show();
			
		}

	}
	
	public boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        return true;
	    }
	    return false;
	}

	private void shareBitmap(Bitmap result) {
		try {
			Intent shareIntent = new Intent(
					Intent.ACTION_SEND);

			String path = Images.Media.insertImage(
					this.getContentResolver(),
					result,
					String.valueOf(new Date().getTime()),
					null);
			Uri screenshotUri = Uri.parse(path);

			String content = "Shared from #FunnyBook#";
			shareIntent.putExtra(Intent.EXTRA_STREAM,
					screenshotUri);
			shareIntent.setType("image/*");
			shareIntent.putExtra("sms_body", content);
			shareIntent
					.putExtra(Intent.EXTRA_TEXT, content);
			this.startActivity(shareIntent);
		} catch (Exception e) {
			Log.e("feec share", e.getMessage());
		}
	}
	
	
	
}

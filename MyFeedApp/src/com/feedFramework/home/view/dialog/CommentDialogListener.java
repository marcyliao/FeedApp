package com.feedFramework.home.view.dialog;

public interface CommentDialogListener {
	void onSuccessfullySubmit();
	void onCancel();
}

package com.feedFramework.home.view.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.feedFramework.R;
import com.feedFramework.entity.Comment;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.impl.CommentServiceImpl;

public class AddCommentDialog extends Dialog{

	private long feedId;
	private long userId;
	
	private CommentDialogListener dialogListener;
	
	public AddCommentDialog(Context context, long feedId, long userId, CommentDialogListener listener) {
		super(context);
		this.setTitle("New Comment");
		this.setContentView(R.layout.dialog_add_comment);
		
		this.feedId = feedId;
		this.userId = userId;
		dialogListener = listener;
		
		Button cancelButton = (Button) this.findViewById(R.id.buttonCancel);
		cancelButton.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				dismiss();
				if(dialogListener != null) {
					dialogListener.onCancel();
				}
			}
		});
		

		// obtain the user name from preference
		EditText editTextNickName = (EditText) findViewById(R.id.editTextNickName);
		editTextNickName.setText(Setting.getUserName(getContext()));
		
		Button submitButton = (Button) this.findViewById(R.id.buttonSubmit);
		submitButton.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				EditText editTextComment = (EditText) findViewById(R.id.editTextComment);
				EditText editTextNickName = (EditText) findViewById(R.id.editTextNickName);
				
				String comment = editTextComment.getText().toString();
				String nickName = editTextNickName.getText().toString();
				
				// validate the user's input
				if(comment==null || comment.equals("")) {
					Toast.makeText(getContext(), "Comment must not be blank.", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(comment.length()<3 || comment.length()>400) {
					Toast.makeText(getContext(), "Comment must be longer than 3 characters and shoter than 400 characters.", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(nickName==null || nickName.equals("")) {
					Toast.makeText(getContext(), "Name must not be blank.", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(nickName.length()<3 || nickName.length()>20) {
					Toast.makeText(getContext(), "Name must be longer than 3 characters and shoter than 20 characters", Toast.LENGTH_SHORT).show();
					return;
				}
				
				// create comment
				Comment c = new Comment();
				c.setContent(comment);
				c.setUserName(nickName);
				c.setUserId(AddCommentDialog.this.userId);
				c.setFeedId(AddCommentDialog.this.feedId);
				
				// store user nick name
				Setting.setUserName(getContext(), nickName);
				
				// submit comment
				LoadingSubmitTask submitingTask = new LoadingSubmitTask(c);
				submitingTask.execute();
			}
		});
	}
	
	private class LoadingSubmitTask extends AsyncTask<Void, Void, Integer>{
		
        ProgressDialog Asycdialog = new ProgressDialog(getContext());
        Comment comment;
        
        int SUCCESS = 0;
        int ERROR = 1;
        
        public LoadingSubmitTask(Comment c) {
        	super();
        	comment = c;
        }

        @Override
        protected void onPreExecute() {
            //set message of the dialog
            Asycdialog.setMessage("Loading...");
            //show dialog
            Asycdialog.show();
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... arg0) {

			try {
				new CommentServiceImpl().addComment(comment);
				return SUCCESS;
			} catch (NetworkException e) {
				return ERROR;
			}
        }

        @Override
        protected void onPostExecute(Integer result) {
        	if(SUCCESS == result) {
        		Asycdialog.dismiss();
        		AddCommentDialog.this.dismiss();
        		Toast.makeText(getContext(), "New comment submitted", Toast.LENGTH_SHORT).show();
        		if(dialogListener != null) {
        			dialogListener.onSuccessfullySubmit();
        		}
        	}
        	else {
        		Asycdialog.dismiss();
        		Toast.makeText(getContext(), "No Internet connection..", Toast.LENGTH_SHORT).show();
        	}

            super.onPostExecute(result);
        }

}

	

}

package com.feedFramework.home.view.dialog;

import com.feedFramework.R;
import com.feedFramework.JokeOfTheDay.AlarmReceiver;
import com.feedFramework.preference.Setting;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class NotificationSettingDialog extends Dialog {
	
	private CheckBox notificationSetting;
	private Button cancelButton;
	private Button submitButton;
	private Context mContext;
	
	public NotificationSettingDialog(Context context) {
		super(context);
		

		this.setTitle("Notification");
		this.setContentView(R.layout.dialog_change_notification_setting);
		
		this.mContext = context;
		
		notificationSetting = (CheckBox) this.findViewById(R.id.checkBoxJokeOfDayPreference);
		notificationSetting.setChecked(Setting.isFeedOfTheDayEnabled(mContext));
		
		cancelButton = (Button) this.findViewById(R.id.buttonCancel);
		cancelButton.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		

		submitButton = (Button) this.findViewById(R.id.buttonSubmit);
		submitButton.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				if(notificationSetting.isChecked() && !Setting.isFeedOfTheDayEnabled(mContext)) {
					AlarmReceiver.startAlarm(mContext);
					Setting.setFeedOfTheDayEnable(mContext,true);
				}
				else if (!notificationSetting.isChecked() && Setting.isFeedOfTheDayEnabled(mContext)) {
					AlarmReceiver.cancelAlarm(mContext);
					Setting.setFeedOfTheDayEnable(mContext,false);
				}
				
				dismiss();
			}
		});
	}


}

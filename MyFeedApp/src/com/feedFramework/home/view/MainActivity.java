package com.feedFramework.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.appbrain.AppBrain;
import com.feedFramework.R;
import com.feedFramework.entity.Feed;
import com.feedFramework.home.view.fragment.MenuFragment;
import com.feedFramework.home.view.fragment.FeedListFragment.AllFragment;
import com.feedFramework.home.view.fragment.FeedListFragment.FavouriteFragment;
import com.feedFramework.home.view.fragment.FeedListFragment.MySelectionsFragment;
import com.feedFramework.preference.Setting;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

public class MainActivity extends SlidingFragmentActivity implements OnMenuMessageListener{

	protected Fragment mFrag;
	private SlidingMenu menu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Set the Main view
		setContentView(R.layout.content_frame);
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.content_frame, new AllFragment())
			.commit();

		// Set the Behind View
		setBehindContentView(R.layout.menu_frame);
		if (savedInstanceState == null) {
			FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
			mFrag = new MenuFragment();
			t.replace(R.id.menu_frame, mFrag);
			t.commit();
		} else {
			mFrag = (Fragment)this.getSupportFragmentManager().findFragmentById(R.id.menu_frame);
		}

		// Customize the SlidingMenu
		menu = getSlidingMenu();
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		
		// Fix the auto scroll down bug
		menu.setOnClosedListener(new OnClosedListener(){
			@Override
			public void onClosed() {
				((MenuFragment)mFrag).scrollToTop();
			}
		});

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public void onBackPressed() {
		if (menu.isMenuShowing()) {
			menu.showContent();
		} else {
			double seed = Math.random();
			Log.v("seed",""+seed);
			if(Setting.isFree()) {
				AppBrain.getAds().showInterstitial(this);
				finish();
			}
			else {
				moveTaskToBack(true);
			}
		}
	}

	@Override
	public void OnLoadAllLatest() {
		menu.showContent();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, new AllFragment()).commit();

	}

	@Override
	public void OnLoadMySelected() {
		menu.showContent();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, new MySelectionsFragment()).commit();
	}

	@Override
	public void OnLoadFavourite() {
		menu.showContent();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, new FavouriteFragment()).commit();

	}
	
	public void showMenu() {
		menu.showMenu();
	}
	
}


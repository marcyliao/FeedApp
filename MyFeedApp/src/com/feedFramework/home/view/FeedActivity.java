package com.feedFramework.home.view;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.presenter.target.SimpleTarget;
import com.feedFramework.R;
import com.feedFramework.entity.Comment;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.home.view.dialog.AddCommentDialog;
import com.feedFramework.home.view.dialog.CommentDialogListener;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.CommentServiceImpl;
import com.feedFramework.service.impl.FeedServiceImpl;
import com.utility.StretchedListView;
import com.utility.Utility;

public class FeedActivity extends SherlockActivity {

	private Feed feed;

	private ViewHolderItem viewHolder = new ViewHolderItem();
	private Context context = this;

	// for comments
	private StretchedListView listViewComments;
	private CommetListAdapter commentListAdapter;

	private TextView textViewCommentsMsg;
	private ProgressBar progressWhenLoadingComments;
	private View layoutWhenLoadingComments;

	private ImageButton addCommentButton;

	class ViewHolderItem {
		View ad;
		
		TextView content;
		TextView userNickName;
		TextView feedCreateTime;
		ImageView imageViewAvata;
		ImageView imageViewContent;

		TextView textViewAddFavourite;
		TextView textViewLike;
		TextView textViewUnlike;

		ImageView imgFavourite;
		ImageView imgLike;
		ImageView imgUnlike;

		LinearLayout buttonFavourite;
		LinearLayout buttonShare;
		LinearLayout buttonLike;
		LinearLayout buttonUnlike;

		View feedView;
		View contentContainer;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed);

		this.getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(0xff3f5ca9));
		
		findViewById(R.id.operationBar).setBackgroundColor(0xff3f5ca9);

		feed = (Feed) getIntent().getExtras().get("feed");

		initWidgets();

		startLoadingComments();

	}

	private void startLoadingComments() {

		Utility.executeAsyncTask(new AsyncTask<Object, Object, Integer>() {

			final static int NO_NETWORK = -1;
			final static int SUCCESS = 0;

			@Override
			protected void onPostExecute(Integer result) {

				if (result == SUCCESS) {
					if (commentListAdapter.getComments() != null
							&& commentListAdapter.getComments().size() != 0) {
						layoutWhenLoadingComments.setVisibility(View.GONE);
						listViewComments.setVisibility(View.VISIBLE);
						commentListAdapter.notifyDataSetChanged();
					} else {
						progressWhenLoadingComments.setVisibility(View.GONE);
						textViewCommentsMsg.setVisibility(View.VISIBLE);
						textViewCommentsMsg.setText("No comment yet..");
					}
				} else if (result == NO_NETWORK) {
					progressWhenLoadingComments.setVisibility(View.GONE);
					textViewCommentsMsg.setVisibility(View.VISIBLE);
					String msg = "No Internet connection..";
					textViewCommentsMsg.setText(msg);
				}
			}

			@Override
			protected Integer doInBackground(Object... params) {

				List<Comment> comments;
				try {
					comments = new CommentServiceImpl().getComments(feed
							.getId());
					commentListAdapter.setComments(comments);
					return SUCCESS;
				} catch (NetworkException e1) {
					return NO_NETWORK;
				}

			}
		});

	}

	private void intiCommentWidgets() {

		// comments
		listViewComments = (StretchedListView) findViewById(R.id.listViewComments);
		listViewComments.setVisibility(View.GONE);

		layoutWhenLoadingComments = findViewById(R.id.layoutWhenLoadingComment);
		layoutWhenLoadingComments.setVisibility(View.VISIBLE);

		textViewCommentsMsg = (TextView) findViewById(R.id.textViewCommentsMsg);
		textViewCommentsMsg.setVisibility(View.GONE);

		progressWhenLoadingComments = (ProgressBar) findViewById(R.id.progressBarWhenLoadingComments);
		progressWhenLoadingComments.setVisibility(View.VISIBLE);

		TextView textViewCommentsNum = (TextView) findViewById(R.id.textViewCommentNum);
		commentListAdapter = new CommetListAdapter(context, textViewCommentsNum);
		listViewComments.setAdapter(commentListAdapter);

		addCommentButton = (ImageButton) findViewById(R.id.buttonAddComment);
		addCommentButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AddCommentDialog dialog = new AddCommentDialog(context, feed
						.getId(), feed.getAuthorId(),
						new CommentDialogListener() {

							@Override
							public void onSuccessfullySubmit() {
								intiCommentWidgets();
								startLoadingComments();
							}

							@Override
							public void onCancel() {
								// do nothing
							}

						});
				dialog.show();
			}

		});

	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		Feed feed = (Feed) getIntent().getExtras().get("feed");
		initFeedContentDisplay(viewHolder, feed);
		super.onWindowFocusChanged(hasFocus);
	}

	private void initWidgets() {

		viewHolder.ad = findViewById(R.id.ad);
		
		viewHolder.feedView = findViewById(R.id.feedView);
		viewHolder.contentContainer = findViewById(R.id.containerFeedContent);

		viewHolder.content = (TextView) findViewById(R.id.textViewFeedContent);
		viewHolder.userNickName = (TextView) findViewById(R.id.textViewUserName);
		viewHolder.feedCreateTime = (TextView) findViewById(R.id.textViewPostTime);
		viewHolder.imageViewAvata = (ImageView) findViewById(R.id.imageViewUserAvata);
		viewHolder.imageViewContent = (ImageView) findViewById(R.id.imageViewContent);

		viewHolder.imgFavourite = (ImageView) findViewById(R.id.imageViewFavourite);
		viewHolder.imgLike = (ImageView) findViewById(R.id.imageViewLike);
		viewHolder.imgUnlike = (ImageView) findViewById(R.id.imageViewUnlike);

		viewHolder.buttonFavourite = (LinearLayout) findViewById(R.id.buttonFavourite);
		viewHolder.buttonLike = (LinearLayout) findViewById(R.id.buttonLike);
		viewHolder.buttonUnlike = (LinearLayout) findViewById(R.id.buttonUnlike);

		viewHolder.textViewAddFavourite = (TextView) findViewById(R.id.textViewAddFavourite);
		viewHolder.buttonShare = (LinearLayout) findViewById(R.id.buttonShare);
		viewHolder.textViewLike = (TextView) findViewById(R.id.textViewLike);
		viewHolder.textViewUnlike = (TextView) findViewById(R.id.textViewUnlike);

		initFavourit(viewHolder, feed);
		initLike(viewHolder, feed);
		initUnlike(viewHolder, feed);
		initShareButton(viewHolder, feed);
		intiCommentWidgets();
		initAdd(viewHolder);

	}

	private void initAdd(ViewHolderItem viewHolder) {
		if(Setting.isFree()) {
			viewHolder.ad.setVisibility(View.VISIBLE);
		} 
		else {
			viewHolder.ad.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		return true;
	}

	// Init the display of the feed in the list
	private void initFeedContentDisplay(final ViewHolderItem viewHolder,
			final Feed feed) {

		if (feed.getContentImageUrl() != null
				&& !feed.getContentImageUrl().equals("")) {

			// init
			ViewGroup.LayoutParams params = viewHolder.imageViewContent.getLayoutParams();
			
			Display display = getActivity().getWindowManager().getDefaultDisplay();
	        final int screenHeight = display.getHeight();	
	        final int screenWidth = display.getWidth();
			params.height= (int) (screenHeight*0.35);
			params.width = (int) (screenWidth-20);
			viewHolder.imageViewContent.setLayoutParams(params);
			viewHolder.imageViewContent.setImageResource(R.drawable.subimage);
			// init ends

			Glide.load(feed.getContentImageUrl()).into(new SimpleTarget() {

				@Override
				public void onImageReady(Bitmap source) {

					float width = source.getWidth();
					float height = source.getHeight();
					
					float ratio = width/height;
					ViewGroup.LayoutParams params = viewHolder.imageViewContent.getLayoutParams();
					params.height = (int) (params.width/ratio);
					
			        viewHolder.imageViewContent.setLayoutParams(params);
					viewHolder.imageViewContent.setImageBitmap(source);

					viewHolder.imageViewContent
							.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									Intent i = new Intent(getActivity(),
											LargeImageDisplayActivity.class);
									i.putExtra("id", feed.getId());
									startActivity(i);
								}
							});
					
					viewHolder.imageViewContent.setImageBitmap(source);

				}

				@Override
				protected int[] getSize() {
					return new int[] { 400, 400 };
				}

			}).with(context);

		} else {
			viewHolder.imageViewContent.setVisibility(View.GONE);
		}

		if (feed.getContent() != null && !feed.getContent().equals("")) {
			viewHolder.content.setVisibility(View.VISIBLE);
			viewHolder.content.setText(feed.getContent());
		} else {
			viewHolder.content.setVisibility(View.GONE);
		}

		Glide.load(feed.getCategoryIcon()).into(viewHolder.imageViewAvata);
		viewHolder.userNickName.setText(feed.getCategoryName());

		viewHolder.feedCreateTime.setText(Utility.getTimeDescription(feed
				.getCreateTime()));
	}

	@Override
	public void onBackPressed() {
		finish();
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	private void initUnlike(ViewHolderItem viewHolder, final Feed feed) {
		// Init the function, button and UI of adding like.

		final ImageView imgUnlike = viewHolder.imgUnlike;
		final LinearLayout buttonUnlike = viewHolder.buttonUnlike;
		final TextView unlikeText = viewHolder.textViewUnlike;

		buttonUnlike.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				buttonUnlike.setClickable(false);

				Utility.executeAsyncTask(new AsyncTask<Object, Object, Feed>() {

					@Override
					protected void onPostExecute(Feed result) {

						buttonUnlike.setClickable(true);

						if (result == null)
							return;

						FeedService fs = new FeedServiceImpl();
						if (!fs.hasCacheFeed(context, FeedService.UNLIKE,
								feed.getId())) {
							fs.addCacheFeed(context, FeedService.UNLIKE, feed);
							imgUnlike
									.setImageResource(R.drawable.ic_unlike_selected);
						} else {
							fs.removeCacheFeed(context, FeedService.UNLIKE,
									feed.getId());
							imgUnlike
									.setImageResource(R.drawable.ic_unlike_unselected);
						}

						unlikeText.setText(String.valueOf(result.getNumUnlike()));
						super.onPostExecute(result);
					}

					@Override
					protected Feed doInBackground(Object... params) {
						FeedService fs = new FeedServiceImpl();
						try {
							if (!fs.hasCacheFeed(context, FeedService.UNLIKE,
									feed.getId())) {
								return fs.addUnlike(feed.getId());
							} else {
								return fs.decreaseUnlike(feed.getId());
							}
						} catch (NetworkException e) {
							return null;
						}
					}
				});
			}
		});

		// Check whether the feed is already added to favourite, decide the
		// display.
		FeedService fs = new FeedServiceImpl();
		if (fs.hasCacheFeed(context, FeedService.UNLIKE, feed.getId())) {
			imgUnlike.setImageResource(R.drawable.ic_unlike_selected);
		} else {
			imgUnlike.setImageResource(R.drawable.ic_unlike_unselected);
		}

		if (feed.getNumUnlike() == 0) {
			unlikeText.setText("Disike");
		} else {
			unlikeText.setText(String.valueOf(feed.getNumUnlike()));
		}
	}

	private void initLike(ViewHolderItem viewHolder, final Feed feed) {
		// Init the function, button and UI of adding like.

		final ImageView imgLike = viewHolder.imgLike;
		final LinearLayout buttonLike = viewHolder.buttonLike;
		final TextView likeText = viewHolder.textViewLike;

		buttonLike.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				buttonLike.setClickable(false);

				Utility.executeAsyncTask(new AsyncTask<Object, Object, Feed>() {

					@Override
					protected void onPostExecute(Feed result) {

						buttonLike.setClickable(true);

						if (result == null)
							return;

						FeedService fs = new FeedServiceImpl();
						if (!fs.hasCacheFeed(context, FeedService.LIKE,
								feed.getId())) {
							fs.addCacheFeed(context, FeedService.LIKE, feed);
							imgLike.setImageResource(R.drawable.ic_like_selected);
						} else {
							fs.removeCacheFeed(context, FeedService.LIKE,
									feed.getId());
							imgLike.setImageResource(R.drawable.ic_like_unselected);
						}

						likeText.setText(String.valueOf(result.getNumLike()));
						super.onPostExecute(result);
					}

					@Override
					protected Feed doInBackground(Object... params) {
						FeedService fs = new FeedServiceImpl();
						try {
							if (!fs.hasCacheFeed(context, FeedService.LIKE,
									feed.getId())) {
								return fs.addLike(feed.getId());
							} else {
								return fs.decreaseLike(feed.getId());
							}
						} catch (NetworkException e) {
							return null;
						}
					}
				});
			}
		});

		// Check whether the feed is already added to favourite, decide the
		// display.
		FeedService fs = new FeedServiceImpl();
		if (fs.hasCacheFeed(context, FeedService.LIKE, feed.getId())) {
			imgLike.setImageResource(R.drawable.ic_like_selected);
		} else {
			imgLike.setImageResource(R.drawable.ic_like_unselected);
		}

		if (feed.getNumLike() == 0) {
			likeText.setText("Like");
		} else {
			likeText.setText(String.valueOf(feed.getNumLike()));
		}

	}

	private void initFavourit(ViewHolderItem viewHolder, final Feed feed) {
		// Init the function, button and UI of adding favourite.

		final ImageView imgFavourite = viewHolder.imgFavourite;
		final LinearLayout buttonFavourite = viewHolder.buttonFavourite;

		buttonFavourite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				FeedService fs = new FeedServiceImpl();
				if (!fs.hasCacheFeed(context, FeedService.FAVOURITE,
						feed.getId())) {
					fs.addCacheFeed(context, FeedService.FAVOURITE, feed);
					imgFavourite.setImageResource(R.drawable.icon_favorite);
				} else {
					fs.removeCacheFeed(context, FeedService.FAVOURITE,
							feed.getId());
					imgFavourite.setImageResource(R.drawable.icon_unfavorite);
				}
			}

		});

		// Check whether the feed is already added to favourite, decide the
		// display.
		FeedService fs = new FeedServiceImpl();
		if (fs.hasCacheFeed(context, FeedService.FAVOURITE, feed.getId())) {
			imgFavourite.setImageResource(R.drawable.icon_favorite);
		} else {
			imgFavourite.setImageResource(R.drawable.icon_unfavorite);
		}
	}

	private void initShareButton(final ViewHolderItem viewHolder,
			final Feed feed) {

		viewHolder.buttonShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startShareTask(viewHolder);
			}

			private void startShareTask(final ViewHolderItem viewHolder) {
				if (feed.getContentImageUrl() != null
						&& !feed.getContentImageUrl().equals("")) {
					Glide.load(feed.getContentImageUrl()).into(new SimpleTarget() {

						@Override
						public void onImageReady(Bitmap source) {
							shareBitmap(source);
							
						}

						@Override
						protected int[] getSize() {
							return new int[]{400,400};
						}
						
					}).with(context);
					
				} else {
					new AsyncTask<Object, Object, Bitmap>() {

						@Override
						protected void onPostExecute(Bitmap result) {
							shareBitmap(result);
							super.onPostExecute(result);
						}

						@Override
						protected Bitmap doInBackground(Object... params) {
							viewHolder.feedView.setDrawingCacheEnabled(true);
							return viewHolder.feedView.getDrawingCache();
						}

					}.execute();
				}

			}

		});
	}

	private void shareBitmap(Bitmap result) {
		try {
			Intent shareIntent = new Intent(Intent.ACTION_SEND);

			String path = Images.Media.insertImage(getActivity()
					.getContentResolver(), result, String.valueOf(new Date()
					.getTime()), null);
			Uri screenshotUri = Uri.parse(path);

			String content = "Shared from #FunnyBook#";
			shareIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			shareIntent.setType("image/*");
			shareIntent.putExtra("sms_body", content);
			shareIntent.putExtra(Intent.EXTRA_TEXT, content);
			getActivity().startActivity(shareIntent);
		} catch (Exception e) {
			Log.e("feec share", e.getMessage());
		}
	}

	private Activity getActivity() {
		return this;
	}

	public StretchedListView getListViewComments() {
		return listViewComments;
	}

	public void setListViewComments(StretchedListView listViewComments) {
		this.listViewComments = listViewComments;
	}
}

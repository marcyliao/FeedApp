package com.feedFramework.home.view;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.presenter.target.SimpleTarget;
import com.feedFramework.R;
import com.feedFramework.home.view.fragment.FeedListFragment.CategoryFeedsFragment;

public class CategoryFeedsActivity extends SherlockFragmentActivity {
	
	// fix the bug that the fake image view gets collected by gc.
	ImageView dummyImageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_frame);
		
		final Long cateId = getIntent().getLongExtra("cateId",-1);
		final String icon = getIntent().getStringExtra("icon");
		final String name = getIntent().getStringExtra("name");
		
		CategoryFeedsFragment fragment= new CategoryFeedsFragment();
		fragment.setCateId(cateId);
		fragment.setTitle(name);
		
		getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.content_frame, fragment)
			.commit();
		
		
		dummyImageView = new ImageView(this);
		
		
		Glide.load(icon).into(new SimpleTarget(){

			@Override
			public void onImageReady(Bitmap iconBitmap) {
				getSupportActionBar().setIcon(new BitmapDrawable(getResources(),iconBitmap));
				
			}

			@Override
			protected int[] getSize() {
				// TODO Auto-generated method stub
				return new int[] {50,50};
			}}).with(this);
		
	}
}

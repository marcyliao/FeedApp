package com.feedFramework.home.view.fragment.FeedListFragment;

import java.util.LinkedList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.feedFramework.R;
import com.feedFramework.entity.Feed;
import com.feedFramework.home.view.fragment.ContentFragment;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.FeedServiceImpl;
import com.utility.Utility;


public class FavouriteFragment extends ContentFragment {
	private ListView mListView;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	    setHasOptionsMenu(true);
		return inflater.inflate(R.layout.feed_list_simple, null);
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		prepareWidgets();	
		initAd();
		initializeFeedListFromCache();
	}
	
	private void initAd() {
		View ad = getActivity().findViewById(R.id.ad);
		if(!Setting.isFree())
			ad.setVisibility(View.GONE);
		else
			ad.setVisibility(View.VISIBLE);
	}

	private void initializeFeedListFromCache() {
		textViewMessage.setVisibility(TextView.VISIBLE);
		textViewMessage.setText("Loading from storage...");
		mListView.setSelection(0);

		Utility.executeAsyncTask(new AsyncTask<Object, Object, List<Feed>>() {
			@Override
			protected void onPostExecute(List<Feed> result) {

				if (result != null) {
					mFeedList = new LinkedList<Feed>(result);
					mAdapter.notifyDataSetChanged();
				}
				
				if(mFeedList.size() != 0) {
					textViewMessage.setVisibility(TextView.GONE);
				}
				else {
					textViewMessage.setText("Sorry, you don't have any favourite items.");
				}
				
				super.onPostExecute(result);
			}

			@Override
			protected List<Feed> doInBackground(Object... params) {

				FeedService fs = new FeedServiceImpl();

				if (getActivity() != null) {
					return fs.loadCacheFeeds(getActivity(), FeedService.FAVOURITE);
				}
				return null;
			}
		});
	}

	
	private void prepareWidgets() {
		
		// This loading view shows when list view is loading from cache
		textViewMessage = (TextView) getView().findViewById(R.id.textViewLoadCache);
		textViewMessage.setVisibility(TextView.GONE);
		
		mListView = (ListView) getView().findViewById(
				R.id.feed_list);
				
		// register adapter
		mAdapter = new FeedListAdapter(getView().getContext());
		mListView.setAdapter(mAdapter);
	}

	@Override
	protected int getThemeColor() {
		// TODO Auto-generated method stub
		return 0xffce5043;
	}
	
	@Override
	protected String getTitle() {
		// TODO Auto-generated method stub
		return "Favourites";
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}


	@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.none, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}

package com.feedFramework.home.view.fragment.FeedListFragment;

import java.util.LinkedList;
import java.util.List;

import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.exception.NoFeedsAddedException;
import com.feedFramework.exception.NotUsingException;
import com.feedFramework.home.view.fragment.FeedListDisplay;
import com.feedFramework.home.view.fragment.TabContentFragment;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.FeedServiceImpl;

public class AllFragment extends TabContentFragment {

	@Override
	protected void configDisplaysSetting() {
		this.addDisplay("LATEST", getLatestFeedsDisplay())
				.addDisplay("POPULAR TODAY", getTopFeedsDisplay())
				.addDisplay("RANDOM", getRandomFeedsDisplay())
				.addDisplay("MOST COMMENTS", getMostCommentFeedsDisplay());
	}

	private FeedListDisplay getRandomFeedsDisplay() {
		return new FeedListDisplay(FeedService.ALL_RANDOM, "Random") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();
				LinkedList<Feed> tempList = new LinkedList<Feed>(
						fs.loadRandomFeeds(0, NUM_FEED_LOAD_EACH_TIME));

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(),
								FeedService.ALL_RANDOM, tempList);
					}
				} else {
					throw new NoFeedsAddedException();
				}

				return tempList;
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = new LinkedList<Feed>(fs.loadRandomFeeds(
						mFeedList.size(), NUM_FEED_LOAD_EACH_TIME));

				newList.addAll(oldFeeds);

				if (oldFeeds != null && !oldFeeds.isEmpty()
						&& getActivity() != null) {
					fs.addCacheFeeds(getActivity(), FeedService.ALL_RANDOM,
							oldFeeds);
				} else {
					throw new NoFeedsAddedException();
				}
				return newList;
			}
		};
	}

	private FeedListDisplay getLatestFeedsDisplay() {
		return new FeedListDisplay(FeedService.ALL_LATEST, "Latest") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();
				LinkedList<Feed> tempList = new LinkedList<Feed>(
						fs.loadLatestFeeds(NUM_FEED_LOAD_EACH_TIME));

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(),
								FeedService.ALL_LATEST, tempList);
					}
				} else {
					throw new NoFeedsAddedException();
				}

				return tempList;
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();
				Long CreateTimeOfLastFeedInList = getCreateTimeOfLastFeedInList();

				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = new LinkedList<Feed>(fs.loadFeedsBefore(
						CreateTimeOfLastFeedInList, NUM_FEED_LOAD_EACH_TIME));

				newList.addAll(oldFeeds);

				if (oldFeeds != null && !oldFeeds.isEmpty()
						&& getActivity() != null) {
					fs.addCacheFeeds(getActivity(), FeedService.ALL_LATEST,
							oldFeeds);
				} else {
					throw new NoFeedsAddedException();
				}
				return newList;
			}
		};
	}

	private FeedListDisplay getMostCommentFeedsDisplay() {
		return new FeedListDisplay(FeedService.ALL_COMMENT, "Most Comments") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();
				LinkedList<Feed> tempList = new LinkedList<Feed>(
						fs.loadCommentFeeds(0, NUM_FEED_LOAD_EACH_TIME));

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(),
								FeedService.ALL_COMMENT, tempList);
					}
				} else {
					throw new NoFeedsAddedException();
				}

				return tempList;
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = new LinkedList<Feed>(fs.loadCommentFeeds(
						mFeedList.size(), NUM_FEED_LOAD_EACH_TIME));

				newList.addAll(oldFeeds);

				if (oldFeeds != null && !oldFeeds.isEmpty()
						&& getActivity() != null) {
					fs.addCacheFeeds(getActivity(), FeedService.ALL_COMMENT,
							oldFeeds);
				} else {
					throw new NoFeedsAddedException();
				}
				return newList;
			}
		};
	}

	private FeedListDisplay getTopFeedsDisplay() {
		return new FeedListDisplay(FeedService.ALL_POPULAR, "Popular") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();
				LinkedList<Feed> tempList = new LinkedList<Feed>(
						fs.loadPopularFeeds(0, NUM_FEED_LOAD_EACH_TIME));

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(),
								FeedService.ALL_POPULAR, tempList);
					}
				} else {
					throw new NoFeedsAddedException();
				}

				return tempList;
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = new LinkedList<Feed>(fs.loadPopularFeeds(
						mFeedList.size(), NUM_FEED_LOAD_EACH_TIME));

				newList.addAll(oldFeeds);

				if (oldFeeds != null && !oldFeeds.isEmpty()
						&& getActivity() != null) {
					fs.addCacheFeeds(getActivity(), FeedService.ALL_POPULAR,
							oldFeeds);
				} else {
					throw new NoFeedsAddedException();
				}
				return newList;
			}
		};
	}

	@Override
	protected int getThemeColor() {
		return 0xffffae0a;
	}

	@Override
	protected String getTitle() {
		return "All Feeds";
	}

}

package com.feedFramework.home.view.fragment.FeedListFragment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.feedFramework.entity.Category;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.exception.NoFeedsAddedException;
import com.feedFramework.exception.NotUsingException;
import com.feedFramework.home.view.fragment.FeedListDisplay;
import com.feedFramework.home.view.fragment.TabContentFragment;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.FeedServiceImpl;

public class CategoryFeedsFragment extends TabContentFragment {

	private long cateId;

	public long getCateId() {
		return cateId;
	}

	public void setCateId(long cateId) {
		this.cateId = cateId;
	}

	@Override
	protected void configDisplaysSetting() {

		this.addDisplay("LATEST", getLatestFeedsDisplay())
				.addDisplay("POPULAR TODAY", getTopFeedsDisplay())
				.addDisplay("RANDOM", getRandomDisplay())
				.addDisplay("MOST COMMENTS", getMostCommentsFeedsDisplay());
	}

	private FeedListDisplay getRandomDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_RANDOM, "RANDOM") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				List<Feed> tempList = fs.loadRandomFeedsOfCategories(
						mySelections, 0, NUM_FEED_LOAD_EACH_TIME);

				if (tempList == null || tempList.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs
						.loadRandomFeedsOfCategories(mySelections,
								mFeedList.size(), NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds == null || oldFeeds.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				newList.addAll(oldFeeds);
				return newList;
			}
		}.setInitBehaviour(FeedListDisplay.LOAD_NETWORK_INIT);
	}

	private FeedListDisplay getLatestFeedsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_LATEST, "Latest") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				List<Feed> tempList = fs.loadLatestFeedsOfCategories(
						mySelections, NUM_FEED_LOAD_EACH_TIME);

				if (tempList == null || tempList.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);

				Long CreateTimeOfLastFeedInList = getCreateTimeOfLastFeedInList();

				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs.loadBeforeFeedsOfCategories(
						mySelections, CreateTimeOfLastFeedInList,
						NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds == null || oldFeeds.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				newList.addAll(oldFeeds);
				return newList;
			}
		}.setInitBehaviour(FeedListDisplay.LOAD_NETWORK_INIT);
	}

	private FeedListDisplay getTopFeedsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_POPULAR, "Popular") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				List<Feed> tempList = fs.loadPopularFeedsOfCategories(
						mySelections, 0, NUM_FEED_LOAD_EACH_TIME);

				if (tempList == null || tempList.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs
						.loadPopularFeedsOfCategories(mySelections,
								mFeedList.size(), NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds == null || oldFeeds.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				newList.addAll(oldFeeds);
				return newList;
			}
		}.setInitBehaviour(FeedListDisplay.LOAD_NETWORK_INIT);
	}

	private FeedListDisplay getMostCommentsFeedsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_COMMENT,
				"Most Comments") {

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				List<Feed> tempList = fs.loadCommentFeedsOfCategories(
						mySelections, 0, NUM_FEED_LOAD_EACH_TIME);

				if (tempList == null || tempList.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				FeedService fs = new FeedServiceImpl();

				List<Category> mySelections = new ArrayList<Category>();
				Category c = new Category();
				c.setId(cateId);
				mySelections.add(c);
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs
						.loadCommentFeedsOfCategories(mySelections,
								mFeedList.size(), NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds == null || oldFeeds.isEmpty()) {
					throw new NoFeedsAddedException();
				}

				newList.addAll(oldFeeds);
				return newList;
			}
		}.setInitBehaviour(FeedListDisplay.LOAD_NETWORK_INIT);
	}

	@Override
	protected int getThemeColor() {
		return 0xff1aa1e1;
	}

	private String title;

	@Override
	protected String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}

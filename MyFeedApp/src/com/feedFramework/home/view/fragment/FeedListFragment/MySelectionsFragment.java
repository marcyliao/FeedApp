package com.feedFramework.home.view.fragment.FeedListFragment;

import java.util.LinkedList;
import java.util.List;

import com.feedFramework.entity.Category;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.exception.NoFeedsAddedException;
import com.feedFramework.exception.NotUsingException;
import com.feedFramework.home.view.fragment.FeedListDisplay;
import com.feedFramework.home.view.fragment.TabContentFragment;
import com.feedFramework.service.CategoryService;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.CategoryServiceImpl;
import com.feedFramework.service.impl.FeedServiceImpl;

public class MySelectionsFragment extends TabContentFragment{
	
	@Override
	protected void configDisplaysSetting() {
		this.addDisplay("LATEST", getLatestFeedsDisplay()).addDisplay("POPULAR TODAY", getTopFeedsDisplay()).addDisplay("RANDOM",getRandomFeedsDisplay()).addDisplay("MOST COMMENTS", getMostCommentsDisplay());
	}

	private FeedListDisplay getMostCommentsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_COMMENT, "MOST COMMENTS"){

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				List<Feed> tempList = fs.loadCommentFeedsOfCategories(mySelections, 0, NUM_FEED_LOAD_EACH_TIME);

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(), FeedService.MY_SELECTION_COMMENT, tempList);
					}
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				
				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs.loadCommentFeedsOfCategories(mySelections, mFeedList.size(), NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds != null && !oldFeeds.isEmpty() && getActivity() != null) {
					newList.addAll(oldFeeds);
					fs.addCacheFeeds(getActivity(), FeedService.MY_SELECTION_COMMENT, oldFeeds);
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return newList;
			}
		};
	}
	
	private FeedListDisplay getRandomFeedsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_RANDOM, "Random"){

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				List<Feed> tempList = fs.loadRandomFeedsOfCategories(mySelections, 0, NUM_FEED_LOAD_EACH_TIME);

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(), FeedService.MY_SELECTION_RANDOM, tempList);
					}
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				
				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs.loadRandomFeedsOfCategories(mySelections, mFeedList.size(), NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds != null && !oldFeeds.isEmpty() && getActivity() != null) {
					newList.addAll(oldFeeds);
					fs.addCacheFeeds(getActivity(), FeedService.MY_SELECTION_RANDOM, oldFeeds);
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return newList;
			}
		};
	}

	private FeedListDisplay getLatestFeedsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_LATEST, "Latest"){

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				List<Feed> tempList = fs.loadLatestFeedsOfCategories(mySelections,NUM_FEED_LOAD_EACH_TIME);

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(), FeedService.MY_SELECTION_LATEST, tempList);
					}
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				
				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());

				Long CreateTimeOfLastFeedInList = getCreateTimeOfLastFeedInList();
				
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs.loadBeforeFeedsOfCategories(mySelections,CreateTimeOfLastFeedInList, NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds != null && !oldFeeds.isEmpty() && getActivity() != null) {
					newList.addAll(oldFeeds);
					fs.addCacheFeeds(getActivity(), FeedService.MY_SELECTION_LATEST, oldFeeds);
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return newList;
			}
		};
	}
	
	private FeedListDisplay getTopFeedsDisplay() {
		return new FeedListDisplay(FeedService.MY_SELECTION_POPULAR, "Popular"){

			@Override
			public LinkedList<Feed> loadFeedsOnPullDownFromTop()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {

				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				List<Feed> tempList = fs.loadPopularFeedsOfCategories(mySelections, 0, NUM_FEED_LOAD_EACH_TIME);

				if (tempList != null && !tempList.isEmpty()) {
					if (getActivity() != null) {
						fs.resetCacheFeeds(getActivity(), FeedService.MY_SELECTION_POPULAR, tempList);
					}
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return new LinkedList<Feed>(tempList);
			}

			@Override
			public LinkedList<Feed> loadFeedsOnPullUpFromBottom()
					throws NetworkException, NoFeedsAddedException,
					NotUsingException {
				
				FeedService fs = new FeedServiceImpl();

				CategoryService service = new CategoryServiceImpl();
				List<Category> mySelections = service.getSelelctedCategories(getActivity());
				LinkedList<Feed> newList = new LinkedList<Feed>(mFeedList);
				List<Feed> oldFeeds = fs.loadPopularFeedsOfCategories(mySelections, mFeedList.size(), NUM_FEED_LOAD_EACH_TIME);

				if (oldFeeds != null && !oldFeeds.isEmpty() && getActivity() != null) {
					newList.addAll(oldFeeds);
					fs.addCacheFeeds(getActivity(), FeedService.MY_SELECTION_POPULAR, oldFeeds);
				}
				else {
					throw new NoFeedsAddedException();
				}
				
				return newList;
			}
		};
	}

	@Override
	protected int getThemeColor() {
		return 0xffb3c833;
	}
	
	@Override
	protected String getTitle() {
		// TODO Auto-generated method stub
		return "My Categories";
	}

}

package com.feedFramework.home.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.feedFramework.R;
import com.feedFramework.entity.Category;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.home.view.CategoryFeedsActivity;
import com.feedFramework.home.view.OnMenuMessageListener;
import com.feedFramework.home.view.dialog.NotificationSettingDialog;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.CategoryService;
import com.feedFramework.service.impl.CategoryServiceImpl;
import com.utility.StretchedListView;
import com.utility.Utility;

public class MenuFragment extends Fragment {

	
	// update categories messages
	private TextView textViewMySelectionMessage;
	private TextView textViewOtherCategoriesMessage;
	
	// Button widgets
	private View btnAllLatest;
	private View btnMySelected;
	private View btnFavourite;
	
	private View btnAbout;
	private View btnShare;
	private View btnNotification;
	
	private ImageView radioBtnAllLatest;
	private ImageView radioBtnMySelected;
	private ImageView radioBtnFavourite;

	private ImageButton imageButtonRefreshCategories;
	
	// List widgets
	private StretchedListView listViewMySelections;
	private StretchedListView listViewAllCategories;
	private MySelectionListAdapter mSelectionAdapter;
	private OtherCategoryListAdapter otherCategoryAdapter;
	
	// Data
	private List<Category> mySelections;
	private List<Category> otherCategories;
	
	// Main Activity Listener
	private OnMenuMessageListener menuListener;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.drawer_menu, null);
	}

	@Override
	public void onAttach(Activity activity) {
		menuListener = (OnMenuMessageListener)activity;
		super.onAttach(activity);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		prepareWidgets();
		updateCategoriesList();
		
		switchSection(radioBtnAllLatest.getId());
	}

	
	private void prepareWidgets() {
		
		View proVersion = getView().findViewById(R.id.proVersion);
		if(Setting.isFree()){
			proVersion.setVisibility(View.VISIBLE);
		}
		else {
			proVersion.setVisibility(View.GONE);
		}
		
		proVersion.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=com.feedFramework.vip"));
				startActivity(intent);
			}
			
		});
		
		prepareButtons();
		prepareSettingButtons();
		prepareListWidgets();
	}

	private void prepareSettingButtons() {
		
		btnAbout = getView().findViewById(R.id.buttonClearAbout);
		btnAbout.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Toast.makeText(getActivity(), "FunnyBook - Version 1.4", Toast.LENGTH_SHORT).show();
			}
		});
		
		btnShare = getView().findViewById(R.id.buttonShareThisApp);
		btnShare.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				String content = "https://play.google.com/store/apps/details?id=com.feedFramework";
				shareIntent.putExtra("sms_body", content);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(Intent.EXTRA_TEXT, content);
				getActivity().startActivity(shareIntent);
			}
		});
		
		btnNotification = getView().findViewById(R.id.buttonNotification);
		btnNotification.setOnClickListener(new OnClickListener(){

			public void onClick(View v) {
				new NotificationSettingDialog(getActivity()).show();
			}
		});
	}

	private void prepareButtons() {
		this.btnAllLatest = getView().findViewById(R.id.buttonAllLatest);
		this.btnMySelected = getView().findViewById(R.id.buttonMySelected);
		this.btnFavourite = getView().findViewById(R.id.buttonFavourite);
		
		imageButtonRefreshCategories = (ImageButton) getView().findViewById(R.id.imageButtonRefreshCategories);
		
		radioBtnAllLatest = (ImageView) getView().findViewById(R.id.imageViewRadioButtonAllLatest);
		radioBtnMySelected = (ImageView) getView().findViewById(R.id.imageViewRadioButtonMySelected);
		radioBtnFavourite = (ImageView) getView().findViewById(R.id.imageViewRadioButtonFavourite);
		
		btnAllLatest.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				switchSection(radioBtnAllLatest.getId());
				menuListener.OnLoadAllLatest();
			}
		});
		
		btnMySelected.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				switchSection(radioBtnMySelected.getId());
				menuListener.OnLoadMySelected();
			}
		});
		
		btnFavourite.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				switchSection(radioBtnFavourite.getId());
				menuListener.OnLoadFavourite();
			}
		});
		
		imageButtonRefreshCategories.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
				updateCategoriesList();
			}
		});
	}
	
	private void switchSection(int sectionButtonId) {
		radioBtnAllLatest.setImageResource(R.drawable.radio_button_unselected);
		radioBtnMySelected.setImageResource(R.drawable.radio_button_unselected);
		radioBtnFavourite.setImageResource(R.drawable.radio_button_unselected);
		
		((ImageView)getView().findViewById(sectionButtonId)).setImageResource(R.drawable.radio_button_selected);
	}

	private void prepareListWidgets() {
		listViewMySelections = (StretchedListView) getView().findViewById(R.id.listViewMySelection);
		listViewAllCategories = (StretchedListView) getView().findViewById(R.id.listViewOtherCategories);
		
		mSelectionAdapter = new MySelectionListAdapter(getActivity());
		otherCategoryAdapter = new OtherCategoryListAdapter(getActivity());
		
		listViewMySelections.setAdapter(mSelectionAdapter);
		listViewAllCategories.setAdapter(otherCategoryAdapter);
		
		textViewMySelectionMessage = (TextView) getView().findViewById(R.id.textViewMySelectionMessage);
		textViewOtherCategoriesMessage = (TextView) getView().findViewById(R.id.textViewOtherCategoriesMessage);
	}
	
	private void updateCategoriesMessages() {
		if(otherCategories==null || otherCategories.isEmpty()) {
			textViewOtherCategoriesMessage.setVisibility(View.VISIBLE);
			textViewOtherCategoriesMessage.setText("No other categories found");
		}
		else
			textViewOtherCategoriesMessage.setVisibility(View.INVISIBLE);

		if(mySelections==null || mySelections.isEmpty()) {
			textViewMySelectionMessage.setVisibility(View.VISIBLE);
			textViewMySelectionMessage.setText("No selected categories found");
		}
		else
			textViewMySelectionMessage.setVisibility(View.INVISIBLE);
	}
	
	private void updateCategoriesList() {
		
		
		Utility.executeAsyncTask(new AsyncTask<Object,Object,Integer>(){
			
			final static int NO_NETWORK = -1;
			final static int SUCCESS = 0;

			@Override
			protected void onPostExecute(Integer result) {
				
				if(result == null)
					return;
				
				super.onPostExecute(result);
				mSelectionAdapter.notifyDataSetChanged();
				otherCategoryAdapter.notifyDataSetChanged();
				
				updateCategoriesMessages();
				
			}

			

			@Override
			protected Integer doInBackground(Object... params) {
				try {

					CategoryService service = new CategoryServiceImpl();

					Integer result = SUCCESS;
					try {
						service.updateAllCategories(getActivity());
					} catch (NetworkException e) {
						result = NO_NETWORK;
					}

					mySelections = service
							.getSelelctedCategories(getActivity());
					otherCategories = service
							.getNotSelectedCategories(getActivity());

					return result;
				} catch (Exception e) {
					return null;
				}
			}
		});		
	}

	private abstract class CategorySelectionListAdapter extends BaseAdapter {
		protected final Context context;
		protected int operationItemResource;
		
		public CategorySelectionListAdapter(Context context, int iconResource) {
			this.context = context;
			operationItemResource = iconResource;
		}

		@Override
		public void notifyDataSetChanged() {
			// TODO Auto-generated method stub
			super.notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			if(getData() != null)
				return getData().size();
			return 0;
		}

		@Override
		public Category getItem(int position) {
			return getData().get(position);
		}

		@Override
		public long getItemId(int position) {
			return getData().get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View rowView = getRootVIew(parent);

			final Category c = getData().get(position);
			
			TextView textViewItem = (TextView) rowView.findViewById(R.id.textViewTitle);
			textViewItem.setText(c.getName());
			
			ImageButton imageButton = (ImageButton) rowView.findViewById(R.id.buttonOperation);
			imageButton.setImageResource(operationItemResource);
			imageButton.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					onOperationClick(c);
					updateCategoriesMessages();
				}
			});
			
			ImageView imageViewIcon = (ImageView) rowView.findViewById(R.id.imageViewIcon);
			Glide.load(c.getIconLink()).placeholder(R.drawable.subimage).into(imageViewIcon);
			
			View itemContainer = rowView.findViewById(R.id.itemContainer);
			itemContainer.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), CategoryFeedsActivity.class);
					intent.putExtra("cateId", c.getId());
					intent.putExtra("icon", c.getIconLink());
					intent.putExtra("name", c.getName());
					startActivity(intent);
				}
			});
			
			return rowView;
		}

		private View getRootVIew(ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.menu_list_item, parent, false);
			return rowView;
		}
		
		protected abstract void onOperationClick(final Category c);
		protected abstract List<Category> getData();
		
	}
	
	private class MySelectionListAdapter extends CategorySelectionListAdapter {

		public MySelectionListAdapter(Context context) {
			super(context, android.R.drawable.ic_delete);
		}

		@Override
		protected void onOperationClick(Category c) {
			c.setSelected(false);
			mySelections.remove(c);
			otherCategories.add(c);
			
			CategoryService service = new CategoryServiceImpl();
			List<Category> newList = new ArrayList<Category>(mySelections);
			newList.addAll(otherCategories);
			service.storeCategoriesSetting(getActivity(), newList);
			
			mSelectionAdapter.notifyDataSetChanged();
			otherCategoryAdapter.notifyDataSetChanged();
		}

		@Override
		protected List<Category> getData() {
			return mySelections;
		}
	}
	
	private class OtherCategoryListAdapter extends CategorySelectionListAdapter {

		public OtherCategoryListAdapter(Context context) {
			super(context, android.R.drawable.ic_input_add);
		}

		@Override
		protected void onOperationClick(Category c) {
			c.setSelected(true);
			mySelections.add(c);
			otherCategories.remove(c);
			
			CategoryService service = new CategoryServiceImpl();
			List<Category> newList = new ArrayList<Category>(mySelections);
			newList.addAll(otherCategories);
			service.storeCategoriesSetting(getActivity(), newList);
			
			mSelectionAdapter.notifyDataSetChanged();
			otherCategoryAdapter.notifyDataSetChanged();
		}

		@Override
		protected List<Category> getData() {
			return otherCategories;
		}
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	public void scrollToTop(){
		ScrollView layout = (ScrollView) getView().findViewById(R.id.main);
		layout.scrollTo(0, 0);
	}
}

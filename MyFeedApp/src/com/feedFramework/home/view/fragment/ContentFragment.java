package com.feedFramework.home.view.fragment;

import java.util.Date;
import java.util.LinkedList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.presenter.target.SimpleTarget;
import com.feedFramework.R;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.home.view.FeedActivity;
import com.feedFramework.home.view.MainActivity;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.FeedServiceImpl;
import com.utility.Utility;

public abstract class ContentFragment  extends SherlockFragment {
	protected static final int NUM_FEED_LOAD_EACH_TIME = 20;

	
	protected FeedListAdapter mAdapter;
	protected TextView textViewMessage;

	// list of the feeds shown currently
	protected LinkedList<Feed> mFeedList = new LinkedList<Feed>();
	
	@Override
	public void onAttach(Activity activity) {
		SherlockFragmentActivity sherlockActivity = (SherlockFragmentActivity) activity;
		sherlockActivity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getThemeColor()));
		sherlockActivity.getSupportActionBar().setTitle(getTitle());
		
		super.onAttach(activity);
	}

	
	protected abstract int getThemeColor();
	protected abstract String getTitle();
	
	protected Long getCreateTimeOfLastFeedInList() {
		
		Long CreateTimeOfLastFeedInList;
		if(mFeedList != null && !mFeedList.isEmpty()) {
			CreateTimeOfLastFeedInList = mFeedList.getLast().getCreateTime();
		}
		else {
			CreateTimeOfLastFeedInList = 0L;
		}
		return CreateTimeOfLastFeedInList;
	}
	
	protected Long getCreateTimeOfFirstFeedInList() {
		Long CreateTimeOfFirstFeedInList;
		if(mFeedList != null && !mFeedList.isEmpty()) {
			CreateTimeOfFirstFeedInList = mFeedList.get(0).getCreateTime();
		}
		else {
			CreateTimeOfFirstFeedInList = 0L;
		}
		return CreateTimeOfFirstFeedInList;
	}
	
	protected class FeedListAdapter extends BaseAdapter {

		private final Context context;

		public FeedListAdapter(Context context) {
			this.context = context;
		}

		@Override
		public boolean isEnabled(int position) {
		    return false;
		}
		
		@Override
		public int getCount() {
			if (mFeedList == null)
				return 0;
			return mFeedList.size();
		}

		@Override
		public Feed getItem(int position) {
			return mFeedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return mFeedList.get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolderItem viewHolder = new ViewHolderItem();

			if (convertView == null) {

				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.feed_list_item, parent,
						false);
				
				viewHolder.feedView = convertView.findViewById(R.id.feedView);
				viewHolder.contentContainer = convertView.findViewById(R.id.containerFeedContent);
				
				viewHolder.content = (TextView) convertView.findViewById(R.id.textViewFeedContent);
				viewHolder.userNickName = (TextView) convertView.findViewById(R.id.textViewUserName);
				viewHolder.feedCreateTime = (TextView) convertView.findViewById(R.id.textViewPostTime);
				viewHolder.imageViewAvata = (ImageView) convertView.findViewById(R.id.imageViewUserAvata);
				viewHolder.imageViewContent = (ImageView) convertView.findViewById(R.id.imageViewContent);
				
				viewHolder.imgFavourite = (ImageView) convertView.findViewById(R.id.imageViewFavourite);
				viewHolder.imgLike = (ImageView) convertView.findViewById(R.id.imageViewLike);
				viewHolder.imgUnlike = (ImageView) convertView.findViewById(R.id.imageViewUnlike);
				
				viewHolder.buttonFavourite = (LinearLayout) convertView.findViewById(R.id.buttonFavourite);
				viewHolder.buttonLike = (LinearLayout) convertView.findViewById(R.id.buttonLike);
				viewHolder.buttonUnlike = (LinearLayout) convertView.findViewById(R.id.buttonUnlike);
				
				viewHolder.textViewAddFavourite = (TextView) convertView
						.findViewById(R.id.textViewAddFavourite);
				viewHolder.buttonShare = (LinearLayout) convertView
						.findViewById(R.id.buttonShare);
				viewHolder.textViewLike = (TextView) convertView
						.findViewById(R.id.textViewLike);
				viewHolder.textViewUnlike = (TextView) convertView
						.findViewById(R.id.textViewUnlike);


				convertView.setTag(viewHolder);

			} else {
				viewHolder = (ViewHolderItem) convertView.getTag();
			}

			final Feed feed = mFeedList.get(position);

			initFeedContentDisplay(viewHolder, feed);
			initFavourit(viewHolder, feed);
			initLike(viewHolder, feed);
			initUnlike(viewHolder, feed);
			initShareButton(viewHolder, feed);

			return convertView;
		}


		class ViewHolderItem {
			
			TextView content;
			TextView userNickName;
			TextView feedCreateTime;
			ImageView imageViewAvata;
			ImageView imageViewContent;
			
			TextView textViewAddFavourite;
			TextView textViewLike;
			TextView textViewUnlike;
			
			ImageView imgFavourite;
			ImageView imgLike;
			ImageView imgUnlike;

			LinearLayout buttonFavourite;
			LinearLayout buttonShare;
			LinearLayout buttonLike;
			LinearLayout buttonUnlike;

			View feedView;
			View contentContainer;
		}

		// Init the display of the feed in the list
		private void initFeedContentDisplay(final ViewHolderItem viewHolder,final Feed feed) {

			if (feed.getContentImageUrl() != null && !feed.getContentImageUrl().equals("")) {

				viewHolder.imageViewContent.setVisibility(View.VISIBLE);
				
				// init
				ViewGroup.LayoutParams params = viewHolder.imageViewContent.getLayoutParams();
				
				Display display = getActivity().getWindowManager().getDefaultDisplay();
		        final int screenHeight = display.getHeight();	
		        final int screenWidth = display.getWidth();
				params.height= (int) (screenHeight*0.35);
				params.width = (int) (screenWidth-20);
				viewHolder.imageViewContent.setLayoutParams(params);
				viewHolder.imageViewContent.setImageResource(R.drawable.subimage);
				// init ends

				Glide.load(feed.getContentImageUrl()).into(new SimpleTarget() {

					@Override
					public void onImageReady(Bitmap source) {

						
						float width = source.getWidth();
						float height = source.getHeight();
						
						float ratio = width/height;
						ViewGroup.LayoutParams params = viewHolder.imageViewContent.getLayoutParams();
						params.height = (int) (params.width/ratio);
				        int maxHeight = (int) (screenHeight*0.5f);
				        
				        if(params.height > maxHeight)
				        	params.height = maxHeight;

				        viewHolder.imageViewContent.setLayoutParams(params);
						viewHolder.imageViewContent.setImageBitmap(source);
						
					}

					@Override
					protected int[] getSize() {
						return new int[]{400,400};
					}
					
				}).with(context);
				
			} else {
				viewHolder.imageViewContent.setVisibility(View.GONE);
			}
			
			if (feed.getContent() != null && !feed.getContent().equals("")) {
				viewHolder.content.setVisibility(View.VISIBLE);
				viewHolder.content.setText(feed.getContent());
			} else {
				viewHolder.content.setVisibility(View.GONE);
			}

			Glide.load(feed.getCategoryIcon()).placeholder(R.drawable.subimage).into(viewHolder.imageViewAvata);

			viewHolder.userNickName.setText(feed.getCategoryName());
			viewHolder.feedCreateTime.setText(Utility.getTimeDescription(feed.getCreateTime()));
			viewHolder.feedView.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(getActivity(),FeedActivity.class);
					i.putExtra("feed", feed);
					startActivity(i);
				}
			});
		}

		private void initUnlike(ViewHolderItem viewHolder, final Feed feed) {
			// Init the function, button and UI of adding like.

			final ImageView imgUnlike = viewHolder.imgUnlike;
			final LinearLayout buttonUnlike = viewHolder.buttonUnlike;
			final TextView unlikeText = viewHolder.textViewUnlike;

			buttonUnlike.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					buttonUnlike.setClickable(false);
					
					Utility.executeAsyncTask(new AsyncTask<Object, Object, Feed>() {
						
						@Override
						protected void onPostExecute(Feed result) {
							
							buttonUnlike.setClickable(true);
							
							if(result == null)
								return;
							
							FeedService fs = new FeedServiceImpl();
							if (!fs.hasCacheFeed(context, FeedService.UNLIKE,feed.getId())) {
								fs.addCacheFeed(context, FeedService.UNLIKE, feed);
								imgUnlike.setImageResource(R.drawable.ic_unlike_selected);
							} else {
								fs.removeCacheFeed(context, FeedService.UNLIKE,feed.getId());
								imgUnlike.setImageResource(R.drawable.ic_unlike_unselected);
							}
							
							unlikeText.setText(String.valueOf(result.getNumUnlike()));
							super.onPostExecute(result);
						}
						
						@Override
						protected Feed doInBackground(Object... params) {
							FeedService fs = new FeedServiceImpl();
							try {
								if (!fs.hasCacheFeed(context, FeedService.UNLIKE,feed.getId())) {
									return fs.addUnlike(feed.getId());
								} else {
									return fs.decreaseUnlike(feed.getId());
								}
							} catch (NetworkException e) {
								return null;
							}
						}
					});
				}
			});

			// Check whether the feed is already added to favourite, decide the
			// display.
			FeedService fs = new FeedServiceImpl();
			if (fs.hasCacheFeed(context, FeedService.UNLIKE, feed.getId())) {
				imgUnlike.setImageResource(R.drawable.ic_unlike_selected);
			} else {
				imgUnlike.setImageResource(R.drawable.ic_unlike_unselected);
			}
			
			if(feed.getNumUnlike() == 0) {
				unlikeText.setText("Disike");
			}
			else {
				unlikeText.setText(String.valueOf(feed.getNumUnlike()));
			}
		}

		private void initLike(ViewHolderItem viewHolder, final Feed feed) {
			// Init the function, button and UI of adding like.

			final ImageView imgLike = viewHolder.imgLike;
			final LinearLayout buttonLike = viewHolder.buttonLike;
			final TextView likeText = viewHolder.textViewLike;

			buttonLike.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					buttonLike.setClickable(false);
					
					Utility.executeAsyncTask(new AsyncTask<Object, Object, Feed>() {
						
						@Override
						protected void onPostExecute(Feed result) {
							
							buttonLike.setClickable(true);
							
							if(result == null)
								return;
							
							FeedService fs = new FeedServiceImpl();
							if (!fs.hasCacheFeed(context, FeedService.LIKE,feed.getId())) {
								fs.addCacheFeed(context, FeedService.LIKE, feed);
								imgLike.setImageResource(R.drawable.ic_like_selected);
							} else {
								fs.removeCacheFeed(context, FeedService.LIKE,feed.getId());
								imgLike.setImageResource(R.drawable.ic_like_unselected);
							}
							
							likeText.setText(String.valueOf(result.getNumLike()));
							super.onPostExecute(result);
						}
						
						@Override
						protected Feed doInBackground(Object... params) {
							FeedService fs = new FeedServiceImpl();
							try {
								if (!fs.hasCacheFeed(context, FeedService.LIKE,feed.getId())) {
									return fs.addLike(feed.getId());
								} else {
									return fs.decreaseLike(feed.getId());
								}
							} catch (NetworkException e) {
								return null;
							}
						}
					});
				}
			});

			// Check whether the feed is already added to favourite, decide the
			// display.
			FeedService fs = new FeedServiceImpl();
			if (fs.hasCacheFeed(context, FeedService.LIKE, feed.getId())) {
				imgLike.setImageResource(R.drawable.ic_like_selected);
			} else {
				imgLike.setImageResource(R.drawable.ic_like_unselected);
			}
			
			if(feed.getNumLike() == 0) {
				likeText.setText("Like");
			}
			else {
				likeText.setText(String.valueOf(feed.getNumLike()));
			}

		}
		
		private void initFavourit(ViewHolderItem viewHolder, final Feed feed) {
			// Init the function, button and UI of adding favourite.

			final ImageView imgFavourite = viewHolder.imgFavourite;
			final LinearLayout buttonFavourite = viewHolder.buttonFavourite;

			buttonFavourite.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					FeedService fs = new FeedServiceImpl();
					if (!fs.hasCacheFeed(context, FeedService.FAVOURITE,
							feed.getId())) {
						fs.addCacheFeed(context, FeedService.FAVOURITE, feed);
						imgFavourite.setImageResource(R.drawable.icon_favorite);
					} else {
						fs.removeCacheFeed(context, FeedService.FAVOURITE,feed.getId());
						imgFavourite.setImageResource(R.drawable.icon_unfavorite);
					}
				}

			});

			// Check whether the feed is already added to favourite, decide the
			// display.
			FeedService fs = new FeedServiceImpl();
			if (fs.hasCacheFeed(context, FeedService.FAVOURITE, feed.getId())) {
				imgFavourite.setImageResource(R.drawable.icon_favorite);
			} else {
				imgFavourite.setImageResource(R.drawable.icon_unfavorite);
			}
		}
		
		private void initShareButton(final ViewHolderItem viewHolder,
				final Feed feed) {

			viewHolder.buttonShare.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					startShareTask(viewHolder);
				}

				private void startShareTask(final ViewHolderItem viewHolder) {
					if(feed.getContentImageUrl() != null && !feed.getContentImageUrl().equals("")) {
						
						Glide.load(feed.getContentImageUrl()).into(new SimpleTarget() {

							@Override
							public void onImageReady(Bitmap source) {
								shareBitmap(source);
								
							}

							@Override
							protected int[] getSize() {
								return new int[]{400,400};
							}
							
						}).with(context);
					}	
					else {
						new AsyncTask<Object, Object, Bitmap>() {
							
							@Override
							protected void onPostExecute(Bitmap result) {
								shareBitmap(result);
								super.onPostExecute(result);
							}

							
							@Override
							protected Bitmap doInBackground(Object... params) {
								viewHolder.feedView.setDrawingCacheEnabled(true);
								return viewHolder.feedView.getDrawingCache();
							}

						}.execute();
					}
					
					
					
				}

			});
		}
	}
	
	private void shareBitmap(Bitmap result) {
		try {
			Intent shareIntent = new Intent(
					Intent.ACTION_SEND);

			String path = Images.Media.insertImage(
					getActivity().getContentResolver(),
					result,
					String.valueOf(new Date().getTime()),
					null);
			Uri screenshotUri = Uri.parse(path);

			String content = "Shared from #FunnyBook#";
			shareIntent.putExtra(Intent.EXTRA_STREAM,
					screenshotUri);
			shareIntent.setType("image/*");
			shareIntent.putExtra("sms_body", content);
			shareIntent
					.putExtra(Intent.EXTRA_TEXT, content);
			getActivity().startActivity(shareIntent);
		} catch (Exception e) {
			Log.e("feec share", e.getMessage());
		}
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				((MainActivity)getActivity()).showMenu();
		}
		return super.onOptionsItemSelected(item);
	}


}

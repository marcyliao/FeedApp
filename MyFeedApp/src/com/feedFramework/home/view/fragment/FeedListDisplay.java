package com.feedFramework.home.view.fragment;

import java.util.LinkedList;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.exception.NoFeedsAddedException;
import com.feedFramework.exception.NotUsingException;

public abstract class FeedListDisplay {
	
	public static int LOAD_CACHE_INIT = 0;
	public static int LOAD_NETWORK_INIT = 1;
	
	private String tabName;
	private int id; // used as cache table
	private int initBehaviour;
	

	public FeedListDisplay(int id, String tabName) {
		this.tabName = tabName;
		this.id = id;
		initBehaviour = LOAD_CACHE_INIT;
	}


	public String getTabName() {
		return tabName;
	}

	public FeedListDisplay setTabName(String tabName) {
		this.tabName = tabName;
		return this;
	}


	public int getInitBehaviour() {
		return initBehaviour;
	}

	public FeedListDisplay setInitBehaviour(int initBehaviour) {
		this.initBehaviour = initBehaviour;
		return this;
	}

	public int getId() {
		return id;
	}

	public FeedListDisplay setId(int id) {
		this.id = id;
		return this;
	}
	

	public abstract LinkedList<Feed> loadFeedsOnPullDownFromTop() throws NetworkException,NoFeedsAddedException,NotUsingException;
	public abstract LinkedList<Feed> loadFeedsOnPullUpFromBottom() throws NetworkException,NoFeedsAddedException,NotUsingException;

	public int getStoredLocation(Context context) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		return sharedPref.getInt("listLocation-"+id, 0);
	}


	public void storeLocation(int i, Context context) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		sharedPref.edit().putInt("listLocation-"+id, i).commit();
	}

	
}

package com.feedFramework.home.view.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.feedFramework.R;
import com.feedFramework.entity.Feed;
import com.feedFramework.exception.NetworkException;
import com.feedFramework.exception.NoFeedsAddedException;
import com.feedFramework.exception.NotUsingException;
import com.feedFramework.preference.Setting;
import com.feedFramework.service.FeedService;
import com.feedFramework.service.impl.FeedServiceImpl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.utility.Utility;

public abstract class TabContentFragment extends ContentFragment {

	// widgets
	protected PullToRefreshListView mPullRefreshListView;

	// list of tabs
	protected List<String> tabs = new ArrayList<String>();
	
	protected List<Button> tabButtons = new ArrayList<Button>();
	
	protected View msgView;

	// list of displays, the order is corresponding to tabs;
	protected List<FeedListDisplay> displays = new ArrayList<FeedListDisplay>();
	protected int currentDisplay = 0;

	protected abstract String getTitle();
	
	public TabContentFragment addDisplay(String tabName, FeedListDisplay display) {
		tabs.add(tabName);
		displays.add(display);
		return this;
	}

	public FeedListDisplay getCurentDisplay() {
		return displays.get(currentDisplay);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	    setHasOptionsMenu(true);
		return inflater.inflate(R.layout.feed_list_refreshable, null);
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		configDisplaysSetting();
		prepareWidgets();
		prepareDisplayTabs();
		
		initAd();

		reloadList();
	}

	private void initAd() {
		View ad = getActivity().findViewById(R.id.ad);
		if(!Setting.isFree())
			ad.setVisibility(View.GONE);
		else
			ad.setVisibility(View.VISIBLE);
	}

	protected abstract void configDisplaysSetting();

	private void prepareDisplayTabs() {
		LinearLayout tabsLayout = (LinearLayout) getView().findViewById(
				R.id.tabOrderby);

		int size = tabs.size();
		for (int i = 0; i < size; i++) {

			Button btn = new Button(getActivity());
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
					LayoutParams.MATCH_PARENT, 1.0f);
			params.setMargins(1, 0, 0, 1);
			btn.setLayoutParams(params);
			btn.setTextSize(12);
			btn.setHeight(50);
			btn.setBackgroundResource(R.drawable.button_white);
			btn.setText(tabs.get(i));
			btn.setTag(displays.get(i));
			btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FeedListDisplay display = (FeedListDisplay) view.getTag();
					currentDisplay = displays.indexOf(display);

					reloadList();
					refreshTabButtonsUI();

				}
			});
			
			tabButtons.add(btn);
			tabsLayout.addView(btn);
		}
		
		refreshTabButtonsUI();
	}
	
	private void refreshTabButtonsUI(){
		for(int i=0; i<tabs.size(); i++) {
			if(i == currentDisplay) {
				tabButtons.get(i).setTextColor(getThemeColor());
			}
			else {
				tabButtons.get(i).setTextColor(Color.BLACK);
			}
		}
	}

	private void reloadList() {
		if (getCurentDisplay().getInitBehaviour() == FeedListDisplay.LOAD_CACHE_INIT) {
			reLoadFromCache();
		} else if (getCurentDisplay().getInitBehaviour() == FeedListDisplay.LOAD_NETWORK_INIT) {
			reloadFromNetwork();
		}
	}

	private void reloadFromNetwork() {
		textViewMessage.setVisibility(TextView.VISIBLE);
		textViewMessage.setText("Loading from network...");
		mPullRefreshListView.getRefreshableView().setSelection(0);

		new GetFeesTask().execute(GetFeesTask.GET_PULL_DOWN_FEEDS);
	}

	private void reLoadFromCache() {
		textViewMessage.setVisibility(TextView.VISIBLE);
		textViewMessage.setText("Loading from storage...");
		mPullRefreshListView.getRefreshableView().setSelection(0);

		Utility.executeAsyncTask(new AsyncTask<Object, Object, List<Feed>>() {
			@Override
			protected void onPostExecute(List<Feed> result) {

				textViewMessage.setVisibility(TextView.GONE);
				if (result != null && result.size() != 0) {
					mFeedList = new LinkedList<Feed>(result);
					mAdapter.notifyDataSetChanged();
				} 
				else if( result != null && result.size() == 0) {
					reloadFromNetwork();
				}
				
				mPullRefreshListView.onRefreshComplete();
				super.onPostExecute(result);
			}

			@Override
			protected List<Feed> doInBackground(Object... params) {

				FeedService fs = new FeedServiceImpl();

				if (getActivity() != null) {
					return fs.loadCacheFeeds(getActivity(), getCurentDisplay()
							.getId());
				}
				return null;
			}
		});

	}

	private void prepareWidgets() {
		// This loading view shows when list view is loading from cache
		textViewMessage = (TextView) getView().findViewById(
				R.id.textViewLoadCache);
		textViewMessage.setVisibility(TextView.GONE);

		mPullRefreshListView = (PullToRefreshListView) getView().findViewById(
				R.id.pull_refresh_list);
		
		msgView = getView().findViewById(
				R.id.textViewMessage);
		msgView.setVisibility(View.GONE);

		initPullToRefreshList();
	}

	private void initPullToRefreshList() {
		mPullRefreshListView.setMode(Mode.BOTH);
		mPullRefreshListView
				.setOnRefreshListener(new OnRefreshListener2<ListView>() {

					String label = DateUtils.formatDateTime(getView()
							.getContext(), System.currentTimeMillis(),
							DateUtils.FORMAT_SHOW_TIME
									| DateUtils.FORMAT_SHOW_DATE
									| DateUtils.FORMAT_ABBREV_ALL);

					@Override
					public void onPullDownToRefresh(
							PullToRefreshBase<ListView> refreshView) {

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy()
								.setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						Utility.executeAsyncTask(new GetFeesTask(),GetFeesTask.GET_PULL_DOWN_FEEDS);
					}

					@Override
					public void onPullUpToRefresh(
							PullToRefreshBase<ListView> refreshView) {

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy()
								.setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						Utility.executeAsyncTask(new GetFeesTask(),GetFeesTask.GET_PULL_UP_FEEDS);
					}
				});

		// Need to use the Actual ListView when registering for Context Menu
		ListView actualListView = mPullRefreshListView.getRefreshableView();
		registerForContextMenu(actualListView);

		// register adapter
		mAdapter = new FeedListAdapter(getView().getContext());
		actualListView.setAdapter(mAdapter);
	}

	private class GetFeesTask extends
			AsyncTask<Integer, Void, Map<String, Object>> {

		static final int GET_PULL_DOWN_FEEDS = 0;
		static final int GET_PULL_UP_FEEDS = 1;

		@Override
		protected Map<String, Object> doInBackground(Integer... params) {

			Map<String, Object> results = new HashMap<String, Object>();
			LinkedList<Feed> newFeeds = null;
			String msg = null;
			String result = "fail";

			try {
				if (params[0].equals(GET_PULL_DOWN_FEEDS)) {
					newFeeds = getCurentDisplay().loadFeedsOnPullDownFromTop();
					msg = "Latest Feeds Added";
				} else /* if (params[0].equals(GET_NEXT_OLD_FEEDS)) */{
					newFeeds = getCurentDisplay().loadFeedsOnPullUpFromBottom();
					msg = "Feeds Added";
				}

				results.put("list", newFeeds);
				result = "success";

			} catch (NetworkException e) {
				msg = "Network error, please check your internet connection";
			} catch (NoFeedsAddedException e) {
				msg = "No more feeds";
			} catch (NotUsingException e) {
				msg = null;
			}

			results.put("msg", msg);
			results.put("result", result);

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(Map<String, Object> results) {
			if (results.get("result").equals("success")) {
				mFeedList = (LinkedList<Feed>) results.get("list");
				mAdapter.notifyDataSetChanged();
			}

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			// TODO: use cool toast :)
			if (results.get("msg") != null && getActivity() != null
					&& results != null && results.get("msg") != null) {
				Toast.makeText(getActivity(),
						(CharSequence) results.get("msg"), Toast.LENGTH_SHORT)
						.show();
			}

			textViewMessage.setVisibility(View.GONE);
			
			if (mFeedList == null || mFeedList.isEmpty())
				msgView.setVisibility(View.VISIBLE);
			else
				msgView.setVisibility(View.GONE);
			
			super.onPostExecute(results);
		}
	}

	public void refresh() {
		// Manual refresh
		mPullRefreshListView.setRefreshing();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			refresh();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


	
}

package com.feedFramework.config;

import android.support.v4.app.Fragment;

public class SimpleSectionConfig extends SectionConfig {

	private FeedListDisplayConfig feedListConfig;
	
	public SimpleSectionConfig(String name) {
		super(name);
		
		feedListConfig = null;
	}
	
	@Override
	public Fragment getFragment() {
		return null;
	}

	public FeedListDisplayConfig getFeedListConfig() {
		return feedListConfig;
	}

	public SectionConfig setFeedListConfig(FeedListDisplayConfig feedListConfig) {
		this.feedListConfig = feedListConfig;
		return this;
	}

}

package com.feedFramework.config;

import android.support.v4.app.Fragment;

abstract public class SectionConfig {
	
	private String sectionName;
	
	public abstract Fragment getFragment();
	
	public SectionConfig(String name) {
		sectionName = name;
	}
	
	public String getSectionName() {
		return sectionName;
	}
	
	public SectionConfig setSectionName(String sectionName) {
		this.sectionName = sectionName;
		return this;
	}
}

package com.feedFramework.config;

public class FeedListDisplayConfig {
	private String orderBy;
	private String name;
	private String tabText;
	private boolean hasCache;
	private String cacheTableName;
	private int initBehaviour;
	private boolean updatable;

	public static int LOAD_CACHE_INIT = 0;
	public static int LOAD_NETWORK_INIT = 1;
	
	
	
	public FeedListDisplayConfig(String name) {
		
		// Default Value
		hasCache = true;
		this.name = name;
		cacheTableName = name;
		tabText = name;
		orderBy = "create_time";
		initBehaviour = LOAD_CACHE_INIT;
		updatable = true;
	}
	
	public String getOrderBy() {
		return orderBy;
	}
	
	public FeedListDisplayConfig setOrderBy(String orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	
	public String getName() {
		return name;
	}
	
	public FeedListDisplayConfig setName(String name) {
		this.name = name;
		return this;
	}

	public String getTabText() {
		return tabText;
	}

	public FeedListDisplayConfig setTabText(String tabText) {
		this.tabText = tabText;
		return this;
	}

	public String getCacheTableName() {
		return cacheTableName;
	}

	public FeedListDisplayConfig setCacheTableName(String cacheTableName) {
		this.cacheTableName = cacheTableName;
		return this;
	}

	public boolean isHasCache() {
		return hasCache;
	}

	public FeedListDisplayConfig setHasCache(boolean hasCache) {
		this.hasCache = hasCache;
		return this;
	}

	public int getInitBehaviour() {
		return initBehaviour;
	}

	public FeedListDisplayConfig setInitBehaviour(int initBehaviour) {
		this.initBehaviour = initBehaviour;
		return this;
	}

	public boolean isUpdatable() {
		return updatable;
	}

	public FeedListDisplayConfig setUpdatable(boolean updatable) {
		this.updatable = updatable;
		return this;
	}
}

package com.feedFramework.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Setting {

	private static final String FEED_OF_THE_DAY = "feedOfTheDay";
	private static final String USER_NAME = "userName";

	public static void setUserName(Context context, String name){ 
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		sharedPref.edit().putString(USER_NAME, name).commit();
	}
	
	public static String getUserName(Context context){ 
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		return sharedPref.getString(USER_NAME, "");
	}
	
	public static boolean isFeedOfTheDayEnabled(Context context) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		return sharedPref.getBoolean(FEED_OF_THE_DAY, true);
	}
	
	public static void setFeedOfTheDayEnable(Context context, boolean isEnable) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		sharedPref.edit().putBoolean(FEED_OF_THE_DAY, isEnable).commit();
	}
	
	public static boolean isFree() {
		return true;
	}

}

package com.utility;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

public class StretchedListView extends LinearLayout {

	private static final String TAG = StretchedListView.class.getSimpleName();
	private final DataSetObserver dataSetObserver;
	private ListAdapter adapter;

	public StretchedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(LinearLayout.VERTICAL);
		this.dataSetObserver = new DataSetObserver() {
			@Override
			public void onChanged() {
				syncDataFromAdapter();
				super.onChanged();
			}

			@Override
			public void onInvalidated() {
				syncDataFromAdapter();
				super.onInvalidated();
			}
		};
	}

	public void setAdapter(ListAdapter adapter) {
		this.adapter = adapter;
		if (adapter != null) {
			this.adapter.registerDataSetObserver(dataSetObserver);
		} else {
			this.adapter.unregisterDataSetObserver(dataSetObserver);
		}

		syncDataFromAdapter();
	}

	public ListAdapter getAdapter() {
		return adapter;
	}

	private void syncDataFromAdapter() {
		Log.d(TAG, "syncDataFromAdapter() called");
		removeAllViews();
		if (adapter != null) {
			int count = adapter.getCount();
			for (int i = 0; i < count; i++) {
				View view = adapter.getView(i, null, this);
				addView(view);
			}
		}
	}

	public int getCount() {
		return adapter != null ? adapter.getCount() : 0;
	}
}

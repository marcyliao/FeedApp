package com.utility;

import java.net.URI;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.feedFramework.exception.NetworkException;

public class Utility {
	
	public static final long DAY = 24*60*60*1000;
	
	public static void setListViewHeightBasedOnChildren(ListView listView) {
	    ListAdapter listAdapter = listView.getAdapter();
	    if (listAdapter == null) {
	        return;
	    }
	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0) {
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));
	        }
	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        totalHeight += view.getMeasuredHeight();
	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
    
    public static String getStringFromServer(String url) throws NetworkException {

		final HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 10000); // time
																		// out
																		// for
																		// response
		HttpConnectionParams.setSoTimeout(httpParams, 10000); // time out for
																// finishing
																// getting all
																// data

		HttpClient client = new DefaultHttpClient(httpParams);
		HttpGet request;

		try {
			request = new HttpGet(new URI(url));
			HttpResponse response = client.execute(request);

			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String out = EntityUtils.toString(entity,"UTF-8");
					return out;
				}
			}

			throw new NetworkException();

		} catch (Exception e) {
			throw new NetworkException(e.getMessage());
		}
	}
    
    public static String postJsonDataToServer(String url,
			Map<String, String> map) throws NetworkException  {

		try {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		
		for (String key : map.keySet()) {
			nameValuePairs.add(new BasicNameValuePair(key, map.get(key)));
		}

		final HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 10000); // time out for response
		HttpConnectionParams.setSoTimeout(httpParams, 10000); // time out for finishing getting all data

		HttpClient client = new DefaultHttpClient(httpParams);
		HttpPost post = new HttpPost(url);

		String out = null;

			post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
			HttpResponse response = client.execute(post);

			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					out = EntityUtils.toString(entity);
				}
			}
			return out;
		} catch (Exception e) {
			throw new NetworkException();
		}

	}
    
    /*
    public static String getStringFromServer(String link) throws NetworkException {
    	
    	URLConnection con = null;
    	URL url = null;
    	BufferedReader rd = null;
    	
		try {
			url = new URL(link);
			con = url.openConnection();
			Log.d("connection log","try to connect");
			con.setConnectTimeout(10000);
			con.setReadTimeout(10000);
			con.connect();
			Log.d("connection log","connected");
			rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String jsonText = readAll(rd);
			
			return jsonText;
		} catch (Exception e) {
			Log.d("connection log","fail"+e.getMessage());
			throw new NetworkException();
		} finally {
			try {
				if(rd != null)
					rd.close();
			} catch (Exception e) {
				throw new NetworkException();
			}
		}
		
	}
    
    private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}
	*/
    
    @SuppressWarnings("unchecked")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    static public <T> void executeAsyncTask(AsyncTask<T, ?, ?> task) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
      }
      else {
        task.execute();
      }
    }
    
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	static public <T> void executeAsyncTask(AsyncTask<Integer, ?, ?> task,
			Integer param) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, param);
		} else {
			task.execute(param);
		}
	}
	
	public static String getTimeDescription(long time) {

		long currentTime = System.currentTimeMillis();
		long delta = currentTime - time;
		
		if(delta < 120000) // in 2 mins
			return "Just Now";
		if(delta < 3600000) // in 1 hour
			return delta/60000 + " mins ago";
		else if(delta < 24*3600000) // in a day
			return delta/3600000 + " hours ago";
		else if(delta < 2*24*3600000)
			return "Yesterday";
		else if(delta < 14*24*3600000) // in two week
			return delta/(24*3600000) + " days ago";
		else {
			Format formatter = new SimpleDateFormat("yyyy-MM-dd",
					Locale.US);
			String dateString = formatter.format(time);
			return dateString;
		}
	}
}
